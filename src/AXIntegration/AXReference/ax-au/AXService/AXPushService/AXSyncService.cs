﻿using AXPushService.Jobs;
using Common.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AXPushService
{
    public partial class AXSyncService : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AXSyncService));
        private IScheduler scheduler;
        public AXSyncService()
        {
            InitializeComponent();
            this.scheduler = AXJobs.Create();
        }

        protected override void OnStart(string[] args)
        {
            SetupJobs();
        }

        protected override void OnStop()
        {
            this.scheduler.Shutdown();
        }

        protected override void OnPause()
        {
            base.OnPause();
            this.scheduler.PauseAll();
        }


        protected override void OnContinue()
        {
            base.OnContinue();
            this.scheduler.ResumeAll();
        }

        public void SetupJobs()
        {
            this.scheduler.Start();
        }
    }
}
