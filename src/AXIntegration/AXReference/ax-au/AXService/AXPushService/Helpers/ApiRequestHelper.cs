﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXPushService.Helpers
{
    public static class ApiRequestHelper
    {
        public static AXApiRequest NewRequest(string action,bool useDev=false)
        {
            AXApiRequest request=new AXApiRequest()
            {
                RequestID=Guid.NewGuid(),
                Action=action,
                Used=false,
                Date=DateTime.Now
            };
            UmbracoEntities db = new UmbracoEntities();
            if(useDev)
            {
                db = new UmbracoEntities("UmbracoEntitiesDev");
            }
            db.AXApiRequests.Add(request);
            db.SaveChanges();

            return request;
        }
    }
}
