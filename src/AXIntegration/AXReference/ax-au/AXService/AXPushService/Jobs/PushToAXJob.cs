﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using AXPushService.AXProxy;
using System.Threading;
using System.Net.Http;
using Common.Logging;
using System.Net.Mail;
using AXPushService.Helpers;
using System.Text;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;

namespace AXPushService.Jobs
{

    public class PushToAXJob: IJob
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(AXSyncService));

        private string GetOrderProperty(UmbracoEntities db, TeaCommerce_Order order, string alias, string defaultValue)
        {
            var prop = db.TeaCommerce_CustomOrderProperty.Where(y => y.Alias.Equals(alias) && y.TeaCommerce_Order.Id==order.Id).FirstOrDefault();
            if (prop != null) return prop.Value;

            return defaultValue;

        }

        public void Execute(IJobExecutionContext context)
        {
            int storeid = 1; //BritaxUSA dot com
            int statusId = 2; //new orders

            AXJobLog jobLog;
            var db = new UmbracoEntities();

            var toSubmit = new List<AXProxy.OrderSubmission>();
            OrderStatus[] statuses =new OrderStatus[0];
            try
            {
                DateTime oneHourAgo=DateTime.Now.AddHours(-1.0);
                var orders = db.TeaCommerce_Order.Where(o => (o.OrderStatusId == 1 || o.OrderStatusId == 6) && o.DateFinalized <= oneHourAgo && !o.TeaCommerce_CustomOrderProperty.Any(prop => prop.Alias == "AXOrderNumber") && !o.TeaCommerce_CustomOrderProperty.Any(prop => prop.Alias == "NeedsCheckedWithAX") && (!String.IsNullOrEmpty(o.TransactionId) && o.TransactionId != "0")).ToList();
                
                //var orders = db.Fetch<OrderWrapper>(sql);

                AXProxyClient client = new AXProxyClient();


                //See if we have the order flagged as "NeedsCheckedWithAX" in the custom order property. If we do, then we need to check that against ax to see if it has already been submitted, if it has been then we need to update the order accordingly.

                var teaCommerceOrdersToCheck=db.TeaCommerce_CustomOrderProperty.Where(x => x.Alias.Equals("NeedsCheckedWithAX")).ToList();

                List<NeedsCheckedOrder> ordersThatNeedRechecked = new List<NeedsCheckedOrder>();

                foreach (TeaCommerce_CustomOrderProperty x in teaCommerceOrdersToCheck)
                {
                    ordersThatNeedRechecked.Add(
                        new NeedsCheckedOrder()
                            {
                                Order = x.TeaCommerce_Order,
                                OrderType = GetOrderProperty(db,x.TeaCommerce_Order,"OrderType","I"),
                                PaymentMode = GetOrderProperty(db, x.TeaCommerce_Order, "PaymentMode", "")
                            }
                    );
                }

                if (ordersThatNeedRechecked != null && ordersThatNeedRechecked.Any())
                {
                    //Add the orders that came back as still needing ran through ax.
                    orders.AddRange(RecheckOrdersAndUpdateIfNeeded(ordersThatNeedRechecked, client, db));
                }

                //•	Online customer orders will use NCIV01 customer
                //•	Online Advocate orders will use NCADV01 customer

                Log.Debug(String.Format("[PushToAxJob] Processing {0} orders",orders.Count()));
                //create the go between classes for each order
                foreach(var o in orders)
                {

                    var properties = db.TeaCommerce_CustomOrderProperty.Where(p => p.OrderId == o.Id);
                    var lines = db.TeaCommerce_OrderLine.Where(p => p.OrderId == o.Id);

                    string currency = db.TeaCommerce_Currency.Where(c => c.Id == o.CurrencyId).Select(c => c.Name).First();

                    TeaCommerce_CustomOrderProperty shipAddress = properties.FirstOrDefault(x => x.Alias == "shipping_streetAddress") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipCity = properties.FirstOrDefault(x => x.Alias == "shipping_city") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipState = properties.FirstOrDefault(x => x.Alias == "shipping_stateOrProvince") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipZip = properties.FirstOrDefault(x => x.Alias == "shipping_zipCode") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipFirstName = properties.FirstOrDefault(x => x.Alias == "shipping_firstName") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipLirstName = properties.FirstOrDefault(x => x.Alias == "shipping_lastName") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty shipPhone = properties.FirstOrDefault(x => x.Alias == "shipping_phone") ?? new TeaCommerce_CustomOrderProperty();
                    TeaCommerce_CustomOrderProperty email = properties.FirstOrDefault(x => x.Alias == "confirmation_email") ?? new TeaCommerce_CustomOrderProperty() { Value = "" };
                    TeaCommerce_CustomOrderProperty cardType = properties.FirstOrDefault(x => x.Alias == "cardType") ?? new TeaCommerce_CustomOrderProperty() { Value = "" };


                    string globalDiscount = (properties.FirstOrDefault(x => x.Alias == "GlobalDiscount") ?? new TeaCommerce_CustomOrderProperty() { Value = "" }).Value;
                    bool isGlobalDiscount = false;
                    Boolean.TryParse(globalDiscount, out isGlobalDiscount);

                    //IEnumerable<string> discountUPCs = (properties.FirstOrDefault(x => x.Alias == "DiscountedProductUpcs") ?? new PropertiesWrapper()).Value.ToLower().Split(',');
                    TeaCommerce_CustomOrderProperty discountAmount = (properties.FirstOrDefault(x => x.Alias == "DiscountAmount") ?? new TeaCommerce_CustomOrderProperty());
                    TeaCommerce_CustomOrderProperty discountCode = (properties.FirstOrDefault(x => x.Alias == "DiscountCodeClient") ?? new TeaCommerce_CustomOrderProperty());
                    TeaCommerce_CustomOrderProperty shipping = properties.FirstOrDefault(x => x.Alias == "ShippingTotal") ?? new TeaCommerce_CustomOrderProperty();
                    decimal discount=0.0M;
                    Decimal.TryParse(discountAmount.Value,out discount);

                    decimal shippingCost = 0.0M;
                    Decimal.TryParse(shipping.Value, out shippingCost);

                    var advocates = db.Database.SqlQuery<String>("SELECT [Value] FROM [dbo].TeaCommerce_CustomOrderLineProperty WHERE alias='advocateItem' AND Value = 'true' AND OrderLineId IN (SELECT [Id] FROM [dbo].TeaCommerce_OrderLine WHERE OrderId=@p0)", new object[1] { o.Id });

                    string suffix = "NET03";
                    string orderType = "I";
                    string account = String.Format("{0}{1}", (String.IsNullOrEmpty(shipState.Value)) ? "??" : shipState.Value.ToUpper(), suffix);
                    if (advocates.Any())
                    { 
                        account = "NCADV01";
                        orderType = "A";
                    }


                    List<AXProxy.OrderLine> submittedLines = new List<AXProxy.OrderLine>();
                    int count=lines.Count();


                    decimal orderTotalPrice = 0.0M;
                    foreach (var ol in lines)
                    {
                        var lineProps = db.TeaCommerce_CustomOrderLineProperty.Where(lp => lp.OrderLineId == ol.Id);

                        TeaCommerce_CustomOrderLineProperty upc = lineProps.FirstOrDefault(x => x.Alias == "upc") ?? new TeaCommerce_CustomOrderLineProperty() { Value = "__none__" };
                        TeaCommerce_CustomOrderLineProperty lineDiscountAmountProp = lineProps.FirstOrDefault(x => x.Alias == "DiscountAmount") ?? new TeaCommerce_CustomOrderLineProperty() { Value = "__none__" };
                        decimal lineDiscountAmount;
                        decimal disc = 0.0M;
                        string code="";
                        if(lineDiscountAmountProp.Value != "__none__" && decimal.TryParse(lineDiscountAmountProp.Value, out lineDiscountAmount))
                        {
                            disc = lineDiscountAmount / ol.Quantity;
                        }

                        string sku = (lineProps.FirstOrDefault(x => x.Alias == "modelNumber") ?? new TeaCommerce_CustomOrderLineProperty()).Value;

                       // if (string.IsNullOrEmpty(sku)) sku = ol.Sku;
                        if(!string.IsNullOrEmpty(sku))
                        {
                            submittedLines.Add(new AXProxy.OrderLine()
                            {
                                Quantity = ol.Quantity,
                                Sku = sku,
                                UnitPrice = ol.UnitPriceWithoutDiscounts,
                                Discount = decimal.Round(disc, 2, MidpointRounding.AwayFromZero),
                                DiscountCode = code
                            });

                            orderTotalPrice += (ol.UnitPriceWithoutDiscounts - disc);
                        }
                    }
                    

                    if(submittedLines.Any())
                    {
                        toSubmit.Add(new OrderSubmission()
                        {
                            Account = account,
                            AuthorizationId=o.TransactionId,
                            CurrencyCode = currency,
                            OrderNumber = ""+o.OrderNumber,
                            ShipAddress = shipAddress.Value,
                            ShipStreet = shipAddress.Value,
                            ShipCity = shipCity.Value,
                            ShipState = shipState.Value,
                            ShipZip = shipZip.Value,
                            ShipPhone = shipPhone.Value,
                            ShipName = String.Format("{0} {1}", shipFirstName.Value, shipLirstName.Value),
                            Email = email.Value,
                            PaymentMode= orderTotalPrice <= 0 ? "Z" : cardType.Value,
                            OrderType=orderType,
                            Shipping = decimal.Round(shippingCost, 2, MidpointRounding.AwayFromZero),
                            LineItems = submittedLines.ToArray()                        
                        });
                    }
                    
                    Log.Debug(String.Format("[PushToAxJob] Finished setting up order {0}",o.OrderNumber));
                }
                
                    
                Log.Debug(String.Format("[PushToAxJob] Sending {0} orders to AX, 10 at a time",toSubmit.Count));

                int batchcount = 0;
                var PAGE_SIZE = 10;
                //only send orders if we have some
                while(batchcount<toSubmit.Count)
                {
                    var batch = toSubmit.Skip(batchcount).Take(PAGE_SIZE);
                    Log.Debug(String.Format("[PushToAxJob] Sending orders {0} to {1}", batchcount, batchcount+batch.Count()));

                    //08/20/15 I am adding a try catch in here to trap an error that could come from the AX proxy. If it errors out, we have no way of knowing if any or none of the
                    //orders were placed with AX. To Remedy this, we will mark these orders in teacommerce as "NeedsCheckedWithAX" through a custom order property. This way, when this service runs again,
                    //we can check to see if they already have ax order numbers and then we can use the UpdateOrdersWithAXOrderNumber
                    try
                    {
                        statuses= client.SubmitOrders(batch.ToArray());
                    }
                    catch(Exception e)
                    {
                        Log.Debug(String.Format("[PushToAxJob] failed to send {0} orders, marking them in umbraco now as needing rechecked.", batch.Count()));

                        //Mark these failed orders as needing reran the next time the service runs.
                        if(!MarkOrdersAsNeedRechecked(batch.Select(x => new FailedOrderInfo(){OrderType=x.OrderType, OrderNumber = x.OrderNumber, PaymentMode= x.PaymentMode}), db))
                        {
                            Log.Debug(String.Format("[PushToAxJob] There was a problem submitting orders from batch count {0}, and then there was an error trying to mark them as needing rechecked again.", batchcount));
                        }
                    }

                    Log.Debug(String.Format("[PushToAxJob] Sending {0} orders to Umbraco to flag statuses", statuses.Length));

                    try
                    {
                        //Update orders with the ax order number.
                        List<string> failedOrders = UpdateOrdersWithAXOrderNumber(statuses, db);

                        if(failedOrders.Any())
                        {
                            Log.Debug(String.Format("Failed to update the ax numbers on {0} orders with order numbers {1}", failedOrders.Count, String.Join(",", failedOrders)));
                            if(!MarkOrdersAsNeedRechecked(batch.Select(x => new FailedOrderInfo(){OrderNumber = x.OrderNumber, PaymentMode = x.PaymentMode, OrderType = x.OrderType}).Where(x => failedOrders.Contains(x.OrderNumber)), db))
                            {
                                Log.Debug(String.Format("There was an error marking the orders as needing rechecked after the update ax order number failed."));
                            }
                        }

                        //Update the orders (now with the new ax number)
                        bool success = UpdateOrderStatuses(statuses, false);
                        if(!success)
                        {
                            Log.Debug(String.Format("Could not update order statuses, now trying to mark the orders as needing rechecked."));
                            /*if(!MarkOrdersAsNeedRechecked(batch.Select(x => new FailedOrderInfo(){OrderType=x.OrderType, OrderNumber = x.OrderNumber, PaymentMode= x.PaymentMode}), db))
                            {
                                Log.Debug(String.Format("Could not mark the orders as needing rechecked after the UpdateOrderStatuses failed."));
                            }*/
                        }

                    }
                    catch(Exception ex)
                    {

                        Log.Debug(String.Format("[PushToAxJob] Error sending to umbraco! {0}", ex.Message));

                        if (statuses.Length > 0)
                        {
                            StringBuilder orderstr = new StringBuilder();
                            orderstr.Append("Order#\taxOrder#");
                            foreach (OrderStatus stat in statuses)
                            {
                                orderstr.Append(String.Format("{0}\t{1}", stat.OrderNumber, stat.AxOrderNumber));
                            }
                            Log.Debug(String.Format("[PushToAxJob] Orders that failed:\r\n\r\n{0}", orderstr.ToString()));

                        }
                    }
                    batchcount += PAGE_SIZE;

                }
                

                jobLog = new AXJobLog()
                {
                    Job = "AX Order Submission",
                    Status = "Success",
                    Date = DateTime.Now
                };

            }

            catch (Exception ex)
            {
                /*Thread.Sleep(1000);
                JobExecutionException e2 = new JobExecutionException(ex,true);
                throw e2;*/
                
                jobLog = new AXJobLog()
                {
                    Job = "AX Order Submission",
                    Status = "Failed",
                    Date = DateTime.Now
                };

                SmtpClient client = new SmtpClient();

                string message = "An error occurred while processing orders {0} on {1}:\r\n\r\nError Message: {2}\r\n\r\n{3}";


                MailMessage m = new MailMessage("no-reply@britax.com","support@scyllagroup.com","Britax: Error sending orders",
                    String.Format(message,
                    new object[4]{
                        DateTime.Now.ToShortDateString(),
                        DateTime.Now.ToShortTimeString(),
                        ex.Message,
                        ex.StackTrace
                    })
                );
                m.CC.Add("Kreg.Osborne@britax.com");
                m.CC.Add("William.Staples@britax.com");

                client.Send(m);
            }

            db.AXJobLogs.Add(jobLog);
            db.SaveChanges();
            

        }

        /// <summary>
        ///  This will take the orders and look them up in AX to see if they exist. If they exist, the Ax Order Number and Order status will be updated in our system. If they are not found, they 
        ///  will be returned in a list, indicating that need reran through AX.
        /// </summary>
        /// <param name="orders">The orders that need to be verified against AX</param>
        /// <param name="ax"></param>
        /// <param name="db"></param>
        /// <returns>List of orders that still need submitted.</returns>
        private IEnumerable<TeaCommerce_Order> RecheckOrdersAndUpdateIfNeeded(IEnumerable<NeedsCheckedOrder> orders, AXProxyClient ax, UmbracoEntities db)
        {
            //The orders that were found in AX and need updated in our system.
            List<OrderStatus> ordersThatNeedUpdated = new List<OrderStatus>();

            //Orders that were NOT found in ax and that need to be ran like normal.
            List<TeaCommerce_Order> ordersThatWerentFound = new List<TeaCommerce_Order>();

            foreach(NeedsCheckedOrder order in orders)
            {
                OrderStatus orderStatus = ax.TrackDownOrder(order.OrderType, order.Order.OrderNumber, order.PaymentMode);

                if(orderStatus.Status != "Not Found") //If the status is NOT "Not Found" then we know it was found.
                {
                    ordersThatNeedUpdated.Add(orderStatus);
                }
                else
                {
                    ordersThatWerentFound.Add(order.Order);
                }
            }

            //Now update the ax orders
            if(ordersThatNeedUpdated.Any())
            {
                bool anyFailures = false;
                OrderStatus[] orderStatusesArray = ordersThatNeedUpdated.ToArray();

                //Update the AX Number on the orders.
                List<string> failedOrders = UpdateOrdersWithAXOrderNumber(orderStatusesArray, db);

                if(failedOrders.Any())
                {
                    anyFailures = true;
                    Log.Debug(String.Format("Could not update Orders with AX number after 'Tracking them down'."));
                }

                //Update the order statuses of the orders in TeaCommerce
                bool status = UpdateOrderStatuses(orderStatusesArray, false);
                if(!status)
                {
                    anyFailures = true;
                    Log.Debug(String.Format("Could not updated order status after 'Tracking them down'."));
                }

                if(!anyFailures)
                {
                    //Everything should be sorted out correctly in our system. Remove the property that flags this as needing checked so its not picked up next time.
                    bool markedSuccessfully = MarkOrdersAsNotNeededChecked(ordersThatNeedUpdated.Select(x => x.OrderNumber), db);

                    if(!markedSuccessfully)
                    {
                        Log.Debug("Encountered an error trying to mark the orders as not needing checked anymore.");
                    }
                }
            }

            //now return a list of orders that weren't found
            return ordersThatWerentFound;
        }







        /// <summary>
        /// Removes the order property "NeedsCheckedWithAX" for all of the order numbers given.
        /// </summary>
        /// <param name="orderNumbers">List of order numbers to have this property removed for.</param>
        /// <param name="db">The database that this change will be made for.</param>
        /// <returns>Bool indicating if there were any errors. </returns>
        private bool MarkOrdersAsNotNeededChecked(IEnumerable<string> orderNumbers, UmbracoEntities db)
        {
            bool anyErrors = false;

            foreach(string orderNum in orderNumbers)
            {
                try
                {

                    var prop1 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "NeedsCheckedWithAX");
                    var prop2 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "OrderType");
                    var prop3 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "PaymentMode");

                    if (prop1 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop1);
                        db.SaveChanges();
                    }
                    if (prop2 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop2);
                        db.SaveChanges();
                    }
                    if (prop3 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop3);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    anyErrors = true;
                    Log.Debug(String.Format("Could not deleted order property NeedsCheckedWithAX for order number {0}", orderNum), e);
                }
            }
            return anyErrors;
        }






        /// <summary>
        /// Adds the order line properties "NeedsCheckedWithAx", "OrderType" and "Paymentmode" so that we know that the orders are in a limbo state that we need to fix later.
        /// </summary>
        /// <param name="failedOrders">The list of orders that failed and need to be marked.</param>
        /// <param name="db">The database to make these changes to.</param>
        /// <returns>Bool indicating if there were any errors. </returns>
        private bool MarkOrdersAsNeedRechecked(IEnumerable<FailedOrderInfo> failedOrders, UmbracoEntities db)
        {
            bool anyErrors = false;

            foreach(var failedOrder in failedOrders)
            {
                try
                {
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "NeedsCheckedWithAX", "1");
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "OrderType", failedOrder.OrderType);
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "PaymentMode", failedOrder.PaymentMode);
                }
                catch (Exception e)
                {
                    anyErrors = true;
                    Log.Debug(String.Format("Could not update order with order number {0}", failedOrder.OrderNumber), e);
                }
            }
            return !anyErrors;
        }


        private List<string> UpdateOrdersWithAXOrderNumber(OrderStatus[] statuses, UmbracoEntities db)
        {

            List<string> failedOrders = new List<string>();


            foreach (var status in statuses)
            {
                try
                {
                    if (status.AxOrderNumber == "N/A")
                    {
                        Log.Debug(String.Format("Order failed during submission: {0}, {1}", status.OrderNumber, status.Status));
                    }
                    else
                    {
                        AddOrUpdateOrderProperty(db, status.OrderNumber, "AXOrderNumber", status.AxOrderNumber);
                    }
                }
                catch (Exception e)
                {
                    failedOrders.Add(status.OrderNumber);
                    Log.Debug(String.Format("Could not update order with order number {0} and AX Order number {1}", status.OrderNumber, status.AxOrderNumber), e);
                }

            }

            return failedOrders;
            
    

            /*
            //try
            //{
            var api = ApiRequestHelper.NewRequest("UpdateOrdersWithAXOrderNumbers");
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["HomeUrl"]);

                    var apiEnd = String.Format("Umbraco/Api/AXEntryApi/UpdateOrdersWithAXOrderNumbers?request={0}",api.RequestID);

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var result = client.PostAsJsonAsync(apiEnd, statuses).Result;

                    bool status = result.Content.ReadAsAsync<bool>().Result;

                    return status;
                }
            //}
           // catch (Exception e)
            //{
                //TODO: Log Exception
            //    return false;
            //}
             * */
        }

        private void AddOrUpdateOrderProperty(UmbracoEntities db, string orderNumber, string alias, string value)
        {

            var prop = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNumber && p.Alias == alias);
            if (prop != null)
            {
                prop.Value = value;
                db.Entry(prop).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                prop = new TeaCommerce_CustomOrderProperty()
                {
                    OrderId = db.TeaCommerce_Order.First(o => o.OrderNumber == orderNumber).Id,
                    Alias = alias,
                    Value = value,
                    IsReadOnly = true,
                    ServerSideOnly = true
                };
                db.TeaCommerce_CustomOrderProperty.Add(prop);
                db.SaveChanges();
            }
        }


        /// <summary>
        /// Makes a call to the AXEntryApi/UpdateOrderStatuses, which will update the order status of the TeaCommerce Order.
        /// </summary>
        /// <param name="statuses">The order statuses from AX</param>
        /// <param name="orderNumberIsAXOrderNumber">Need this in here because the get order statuses in the AX proxy returns the Ax Order Number in the Ordernumber field</param>
        /// <returns>True if there were no errors on the API side. False, if there were errors (not necessarily fatal)</returns>
        private bool UpdateOrderStatuses(OrderStatus[] statuses, bool orderNumberIsAXOrderNumber)
        {

            //try
            //{
            var api = ApiRequestHelper.NewRequest("UpdateOrderStatuses");
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["HomeUrl"]);

                    var apiEnd = String.Format("Umbraco/Api/AXEntryApi/UpdateOrderStatuses?request={0}", api.RequestID);

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var requestModel = new { OrderNumberIsAXOrderNumber = orderNumberIsAXOrderNumber, Statuses = statuses };

                    var result = client.PostAsJsonAsync(apiEnd, requestModel).Result;

                    bool status = result.Content.ReadAsAsync<bool>().Result;

                    return status;
                }
            //}
           // catch (Exception e)
           // {
           //     //TODO: Log exception
            //    return false;
           // }
        }

    }

    public class FailedOrderInfo
    {
        public string OrderType { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentMode { get; set; }
    }
}