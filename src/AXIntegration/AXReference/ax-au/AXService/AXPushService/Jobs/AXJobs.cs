﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Configuration;

namespace AXPushService.Jobs
{
    public class AXJobs
    {
        public static IScheduler Create()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

            ITrigger sixAmSixPm = TriggerBuilder.Create().WithCronSchedule("0 0 6,18 1/1 * ? *").Build();//0th minute at 6am & 6pm every day of the month every month for every year, will handle daylight savings automatically.
            ITrigger SevenAmSevenPm = TriggerBuilder.Create().WithCronSchedule("0 0 7,19 1/1 * ? *").Build(); //0th minute at 7am & 7pm every day of the month every month for every year, will handle daylight savings automatically.
            ITrigger hourly = TriggerBuilder.Create().WithSimpleSchedule(s => s.WithIntervalInHours(1).RepeatForever()).Build();

            
            IJobDetail submitOrders = JobBuilder.Create<PushToAXJob>().Build();
            IJobDetail checkInventory = JobBuilder.Create<CheckInventoryJob>().Build();
            IJobDetail updateOrderStatuses = JobBuilder.Create<UpdateStatusesJob>().Build();

            scheduler.ScheduleJob(submitOrders, sixAmSixPm);
            scheduler.ScheduleJob(checkInventory, SevenAmSevenPm);     
            scheduler.ScheduleJob(updateOrderStatuses, hourly);


            return scheduler;
        }
    }
}