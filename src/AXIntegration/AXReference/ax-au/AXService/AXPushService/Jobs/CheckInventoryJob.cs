﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Reflection;
using System.Xml;
using AXPushService.AXProxy;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json;
using System.Web.Http;
using System.IO;
using System.Data.SqlClient;
using Common.Logging;
using AXPushService.Helpers;

namespace AXPushService.Jobs
{
    class ContentIdAndSku
    {
        public int ContentId
        {
            get;
            set;
        }
        public string Sku
        {
            get;
            set;
        }
    }

    public class CheckInventoryJob: IJob
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(AXSyncService));

        public void Execute(IJobExecutionContext context)
        {
            int perrequest = 50;
            AXJobLog jobLog;
            UmbracoEntities db = new UmbracoEntities();
            try
            {

                List<String> products = db.cmsContentXmls.Where(s => s.xml.Contains("nodeTypeAlias=\"ProductVariant\"")).Select(s => s.xml).ToList();

                List<ContentIdAndSku> skus = products.Select(p =>
                {
                    ContentIdAndSku obj = new ContentIdAndSku();
                    int id=-1;
                    var doc = new XmlDocument();
                    doc.LoadXml(p);
                    if (Int32.TryParse(doc.FirstChild.Attributes["id"].Value, out id))
                    {
                        obj.ContentId = id;
                        var model = doc.GetElementsByTagName("modelNumber");
                        if (model.Count > 0)
                        {
                            obj.Sku = model[0].InnerText;
                        }

                    }
                    return obj;
                }).Where(s => !String.IsNullOrEmpty(s.Sku) && !s.Sku.StartsWith("000")).ToList();

                Log.Debug(String.Format("[CheckInventoryJob] Getting inventory for {0} skus", skus.Count));
                var client = new AXProxyClient();

                List<InventoryLevel> levels = new List<InventoryLevel>();
                int count = 1;
                bool success;
                foreach (var content in skus)
                {
                    try{
                        var lvl = client.GetInventoryLevel(content.Sku);
                        lvl.Sku = content.ContentId.ToString(); //we're faking this to be the content id for umbraco speed
                        lvl.Inventory = Math.Max(lvl.Inventory, 0);
                        levels.Add(lvl);
                    }
                    catch(Exception ex)
                    {
                        Log.Debug(String.Format("[CheckInventoryJob] Error getting inventory for sku {0}: {1}", content.Sku, ex.Message));
                    }
                    count++;

                    if (count % perrequest == 0)
                    {
                        Log.Debug(String.Format("[CheckInventoryJob] Sending batch of {0} inventory levels to Umbraco", levels.Count));
                        success = SetInventoryLevelsInUmbraco(levels.ToArray(), ConfigurationManager.AppSettings["HomeUrl"]);
                        success = SetInventoryLevelsInUmbraco(levels.ToArray(), ConfigurationManager.AppSettings["HomeUrlAlt"], true);
                        levels.Clear();
                    }
                }

                client.Close();
                if (levels.Count > 0)
                {
                    Log.Debug(String.Format("[CheckInventoryJob] Sending batch of {0} inventory levels to Umbraco", levels.Count));
                    success = SetInventoryLevelsInUmbraco(levels.ToArray(), ConfigurationManager.AppSettings["HomeUrl"]);
                    success = SetInventoryLevelsInUmbraco(levels.ToArray(), ConfigurationManager.AppSettings["HomeUrlAlt"],true);
                }
                Log.Debug(String.Format("[CheckInventoryJob] Done with inventory."));
                jobLog = new AXJobLog()
                {
                    Job = "AX Inventory Levels",
                    Status = "Success",
                    Date = DateTime.Now
                };

            }
            catch (AggregateException ex)
            {
                Log.Debug(String.Format("[CheckInventoryJob] Error getting inventory: {0}", String.Join(", ", ex.InnerExceptions.Select(s => s.Message).ToArray())));
                jobLog = new AXJobLog()
                {
                    Job = "AX Inventory Levels",
                    Status = "Failed",
                    Date = DateTime.Now
                };
            }
            catch(Exception ex)
            {
                Log.Debug(String.Format("[CheckInventoryJob] Error getting inventory: {0}", ex.Message));
                jobLog = new AXJobLog()
                {
                    Job = "AX Inventory Levels",
                    Status = "Failed",
                    Date = DateTime.Now
                };
            }

            db.AXJobLogs.Add(jobLog);
            db.SaveChanges();
            //TODO: Update stock/out of stock
        }

        private bool SetInventoryLevelsInUmbraco(InventoryLevel[] levels, string homeUrl,bool useDev=false)
        {
            try
            {

                var api = ApiRequestHelper.NewRequest("SetInventory", useDev);
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(homeUrl);
                    //var baseAddress = new Uri(ConfigurationManager.AppSettings["PingUrl"]); 
                    var apiEnd = String.Format("Umbraco/Api/AXEntryApi/SetInventory?request={0}", api.RequestID);

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var task = client.PostAsJsonAsync(apiEnd, levels);
                    task.Wait();

                    var result = task.Result;

                    string status = "";
                    bool success = false;

                    var readtask = result.Content.ReadAsStringAsync();
                    readtask.Wait();

                    status = readtask.Result;
                    Boolean.TryParse(status, out success);
                    return success;
                }

            }
            catch (AggregateException aex)
            {
                RecursivelyLogException(aex);
                return false;
                //throw aex;

            }
            catch (Exception ex)
            {
                Log.Debug(String.Format("[CheckInventoryJob] Error sending inventory to {0}: {1}",homeUrl, ex.Message));
                throw ex;
            }
        }
        
        private void RecursivelyLogException(Exception ex)
        {
            Log.Debug(String.Format("[CheckInventoryJob] Error :{0}\r\n{1}", ex.Message, ex.StackTrace));
            if (ex.InnerException != null)
                RecursivelyLogException(ex.InnerException);

            if(ex is AggregateException)
            {
                AggregateException aex = (AggregateException)ex;
                foreach (Exception iex in aex.InnerExceptions)
                    RecursivelyLogException(iex);

            }

        }
    }
}