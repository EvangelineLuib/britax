﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;

namespace AXProxy
{
    public class AXProxy : IAXProxy
    {
        private AXWebService.SalesOrderServiceClient SalesClient
        {
            get
            {
                AXWebService.SalesOrderServiceClient client = new AXWebService.SalesOrderServiceClient();
                client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                client.ClientCredentials.Windows.ClientCredential.UserName = "BRITAX\yourusername";
                client.ClientCredentials.Windows.ClientCredential.Password = "yourpassword";
                return client;

            }
        }

        private AXInventoryOnHandService.InventoryOnHandServiceClient InventoryClient
        {
            get
            {
                AXInventoryOnHandService.InventoryOnHandServiceClient client = new AXInventoryOnHandService.InventoryOnHandServiceClient();

                client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                client.ClientCredentials.Windows.ClientCredential.UserName = "BRITAX\yourusername";
                client.ClientCredentials.Windows.ClientCredential.Password = "yourpassword";
                return client;

            }
        }



        double IAXProxy.GetTaxRate(string zipCode)
        {
            return 0.0;
        }

        OrderStatus IAXProxy.GetOrderByCustomer(string customerName)
        {
            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            AXWebService.QueryCriteria qc = new AXWebService.QueryCriteria();
            AXWebService.CriteriaElement[] qe = { new AXWebService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record 
            AXWebService.EntityKey[] entityKey = { new AXWebService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "DeliveryName";
            qe[0].Operator = AXWebService.Operator.Equal;
            qe[0].Value1 = customerName;

            qc.CriteriaElement = qe;

            var client = SalesClient;

            try
            {

                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    AXWebService.AxdSalesOrder order = client.read(entityKey);

                    if (order != null && order.SalesTable.Length > 0)
                    {
                        var table = order.SalesTable[0];
                        if (table.SalesStatus.HasValue)
                        {
                            status = table.SalesStatus.Value.ToString();
                        }
                        ship.Add(table.BEHDeliveryNum);
                        foreach (var line in table.SalesLine)
                        {
                            //if one line is delivered, mark the whole order delivered
                            if (line.SalesStatus == AXWebService.AxdEnum_SalesStatus.Delivered)
                                status = line.SalesStatus.ToString();
                            ship.Add(line.BEHDeliveryNum);
                        }

                    }
                }
            }
            catch (Exception)
            {
            }
            return new OrderStatus()
            {
                OrderNumber = customerName,
                Status = status,
                ShippingNumber = ship.Distinct()
            };
        }

        OrderStatus IAXProxy.GetOrderStatus(string orderNumber)
        {
            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            AXWebService.QueryCriteria qc = new AXWebService.QueryCriteria();
            AXWebService.CriteriaElement[] qe = { new AXWebService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record 
            AXWebService.EntityKey[] entityKey = { new AXWebService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "SalesId";
            qe[0].Operator = AXWebService.Operator.Equal;
            qe[0].Value1 = orderNumber;

            qc.CriteriaElement = qe;

            var client = SalesClient;

            try
            {

                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    AXWebService.AxdSalesOrder order = client.read(entityKey);

                    if (order != null && order.SalesTable.Length > 0)
                    {
                        var table = order.SalesTable[0];
                        if (table.SalesStatus.HasValue)
                        {
                            status = table.SalesStatus.Value.ToString();
                        }
                        ship.Add(table.BEHDeliveryNum);
                        foreach (var line in table.SalesLine)
                        {
                            //if one line is delivered, mark the whole order delivered
                            if (line.SalesStatus == AXWebService.AxdEnum_SalesStatus.Delivered)
                                status = line.SalesStatus.ToString();
                            ship.Add(line.BEHDeliveryNum);
                        }

                    }
                }
            }
            catch (Exception)
            {
            }
            return new OrderStatus()
            {
                OrderNumber = orderNumber,
                Status = status,
                ShippingNumber = ship.Distinct()
            };
        }

        OrderStatus[] IAXProxy.GetOrderStatuses(string[] orderNumbers)
        {
            var client = SalesClient;
            List<OrderStatus> statuses = new List<OrderStatus>();
            foreach(string orderId in orderNumbers)
            {
                string status = "Not Found";
                List<String> ship = new List<String>();
                // define QueryCriteria that will be used for finding data in AX
                AXWebService.QueryCriteria qc = new AXWebService.QueryCriteria();
                AXWebService.CriteriaElement[] qe = { new AXWebService.CriteriaElement() };

                // define entity key that will contain value of unique identifier of a record 
                AXWebService.EntityKey[] entityKey = { new AXWebService.EntityKey() };

                // example of criteria element for a query
                qe[0].DataSourceName = "SalesTable";
                qe[0].FieldName = "SalesId";
                qe[0].Operator = AXWebService.Operator.Equal;
                qe[0].Value1 = orderId;

                qc.CriteriaElement = qe;


                try
                {

                    // findKeys service operation will return list of unique fields that meet query criteria
                    entityKey = client.findKeys(qc);
                    if (null != entityKey)
                    {
                        AXWebService.AxdSalesOrder order = client.read(entityKey);

                        if (order != null && order.SalesTable.Length > 0)
                        {
                            var table = order.SalesTable[0];
                            if (table.SalesStatus.HasValue)
                            {
                                status = table.SalesStatus.Value.ToString();
                            }
                            ship.Add(table.BEHDeliveryNum);
                            foreach (var line in table.SalesLine)
                            {
                                ship.Add(line.BEHDeliveryNum);
                            }
                        }
                    }
                    else
                    {
                        //Couldn't find the order number in ax, so it must be deleted, cancel the order on our end
                        status = AXWebService.AxdEnum_SalesStatus.Canceled.ToString();
                    }
                    statuses.Add(new OrderStatus()
                    {
                        OrderNumber = orderId,
                        Status = status,
                        ShippingNumber = ship.Distinct()
                    });
                }
                catch (Exception)
                {
                }
            }
            return statuses.ToArray();
        }

        InventoryLevel[] IAXProxy.GetInventoryLevels(string[] skus)
        {
            // define client
            var client = InventoryClient;

            List<InventoryLevel> levels=new List<InventoryLevel>();

            foreach(string sku in skus)
            {
                AXInventoryOnHandService.QueryCriteria qc = new AXInventoryOnHandService.QueryCriteria();
                AXInventoryOnHandService.CriteriaElement[] qe = { new AXInventoryOnHandService.CriteriaElement() };

                // define entity key that will contain value of unique identifier of a record 
                AXInventoryOnHandService.EntityKey[] entityKey = { new AXInventoryOnHandService.EntityKey() };

                // example of criteria element for a query
                qe[0].DataSourceName = "InventSum";
                qe[0].FieldName = "ItemId";
                qe[0].Operator = AXInventoryOnHandService.Operator.Equal;
                qe[0].Value1 = sku;

                qc.CriteriaElement = qe;


                try
                {

                    // findKeys service operation will return list of unique fields that meet query criteria
                    entityKey = client.findKeys(qc);
                    if (null != entityKey)
                    {
                        var onhand = client.read(entityKey);
                        decimal inventory=onhand.InventSum.Where(i => i.InventDim.Any(d => d.InventLocationId == "DIST")).Sum(i => (i.AvailPhysicalSpecified) ? i.AvailPhysical.Value : 0.0M);
                        levels.Add(new InventoryLevel()
                        {
                            Sku = sku,
                            Inventory = inventory
                        });
                    }
                    else
                    {
                        //Since we couldn't find the Item by Model Number, we know that AX doesn't have this model in its system so we want to 
                        //mark it as out of stock.
                        levels.Add(new InventoryLevel()
                            {
                                Sku = sku,
                                Inventory = 0M
                            });
                    }
                }
                catch(Exception)
                {
                    levels.Add(new InventoryLevel()
                    {
                        Sku = sku,
                        Inventory = -1.0M
                    });
                }
            } 

            return levels.ToArray();
        }

        
        InventoryLevel IAXProxy.GetInventoryLevel(string sku)
        {
            var level = new InventoryLevel() { };
            level.Sku = sku;
            level.Inventory = -1.0M;
            // define client
            var client = InventoryClient;

            // define QueryCriteria that will be used for finding data in AX
            AXInventoryOnHandService.QueryCriteria qc = new AXInventoryOnHandService.QueryCriteria();
            AXInventoryOnHandService.CriteriaElement[] qe = { new AXInventoryOnHandService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record 
            AXInventoryOnHandService.EntityKey[] entityKey = { new AXInventoryOnHandService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "InventSum";
            qe[0].FieldName = "ItemId";
            qe[0].Operator = AXInventoryOnHandService.Operator.Equal;
            qe[0].Value1 = sku;

            qc.CriteriaElement = qe;

            try
            {
                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    var onhand = client.read(entityKey);
                    level.Inventory = onhand.InventSum.Where(i=>i.InventDim.Any(d=>d.InventLocationId=="DIST")).Sum(i => (i.AvailPhysicalSpecified) ? i.AvailPhysical.Value : 0.0M);
                }

            }
            catch(Exception)
            {
                level = new InventoryLevel()
                {
                    Sku = sku,
                    Inventory = -1.0M
                };
            }
            client.Close();

            return level;
           
        }

        OrderStatus[] IAXProxy.SubmitOrders(OrderSubmission[] orders)
        {
            List<OrderStatus> statuses = new List<OrderStatus>();
            var client = SalesClient;
            int dateadder = 1;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) dateadder += 1;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday) dateadder += 2;
            DateTime shipDate = DateTime.Now.AddDays(dateadder);

            OperationContextScope o = new OperationContextScope(client.InnerChannel);

            
            foreach (var order in orders)
            {
                    List<AXWebService.AxdEntity_SalesLine> salesLines = new List<AXWebService.AxdEntity_SalesLine>();

                    // Create an instance of the document class.
                    AXWebService.AxdSalesOrder salesOrder = new AXWebService.AxdSalesOrder();

                    AXWebService.AxdEntity_SalesTable salesTable = new AXWebService.AxdEntity_SalesTable();
                    string endpoint = "bcsi";
                    string shipping = "U2";

                    var splitString = order.OrderNumber.Split('-');
                    string country = "us"; //Default to US

                    if (splitString != null && splitString.Any())
                    {
                        //If the first part of the order number equals us, then its us, else if it equals canada then its ca, else its us.
                        country = splitString[0].Equals("US", StringComparison.InvariantCultureIgnoreCase) ? "us" : splitString[0].Equals("CA", StringComparison.InvariantCultureIgnoreCase) ? "ca" : "us";
                    }

                    //ORDER INFORMATION FROM THE WEBSITE
                    salesTable.CurrencyCode = order.CurrencyCode; //USD
                    salesTable.DeliveryName = order.ShipName; //customer first name and last
                    salesTable.DeliveryAddress = order.ShipAddress; //shipping addreess for order
                    salesTable.DeliveryStreet = order.ShipStreet; //shipping street for order
                    salesTable.DeliveryState = order.ShipState; //shipping state for order
                    salesTable.DeliveryCity = order.ShipCity; //shipping state for order
                    salesTable.DeliveryZipCode = order.ShipZip; //shipping zip code for order
                    salesTable.TEC_DeliveryPhone = order.ShipPhone; //shipping phone for order
                    salesTable.DeliveryCity = order.ShipCity; //Shipping city
                    salesTable.CreditCardProcessorTransactionId = order.AuthorizationId; //authorize.net transaction
                    salesTable.CustAccount = order.Account; //two character state abbrev + NET03, typically (ex: VANET03)
                    salesTable.InvoiceAccount = order.Account; //same as above

                    //If this is a canadian order we want to add some items to the sales table.
                    if (country.Equals("ca", StringComparison.InvariantCultureIgnoreCase))
                    {
                        salesTable.InterCompanyAutoCreateOrders = AXWebService.AxdExtType_InterCompanyAutoCreateOrders.Yes;
                        salesTable.InterCompanyAutoCreateOrdersSpecified = true;
                        salesTable.InterCompanyDirectDelivery = AXWebService.AxdExtType_InterCompanyDirectDelivery.Yes;
                        salesTable.InterCompanyDirectDeliverySpecified = true;
                        endpoint = "bcsl";
                        shipping = "U10";
                    }

                    salesTable.CustomerRef = String.Format("{0}-{1} PT: {2}", order.OrderType, order.OrderNumber, order.PaymentMode); //I-ORDER-16, e.g., followed by the credit card type
                    salesTable.PurchOrderFormNum = order.AuthorizationId; //authorize.net transaction
                    salesTable.SalesName = order.ShipName; //customer first name and last
                    salesTable.Email = order.Email; //confirmation email address

                    salesTable.ReceiptDateRequested = shipDate;
                    salesTable.ReceiptDateRequestedSpecified = true;
                    salesTable.ReceiptDateConfirmed = shipDate;
                    salesTable.ReceiptDateConfirmedSpecified = true;
                    salesTable.ShippingDateRequested = shipDate;
                    salesTable.ShippingDateRequestedSpecified = true;
                    salesTable.ShippingDateConfirmed = shipDate;
                    salesTable.ShippingDateConfirmedSpecified = true;



                    //AX DEFAULTS PER BRITAX DIRECTION
                    //salesTable.BRIBypassDupPOCheck = AXWebService.AxdExtType_BRIBypassDupPOCheck.Yes; //REMOVED PER BRITAX DIRECTION
                    salesTable.SalesType = AXWebService.AxdEnum_SalesType.Sales;
                    salesTable.SalesOriginId = "WEB"; //web order
                    salesTable.DlvMode = shipping; //U2 for US, U10 for CA
                    //salesTable.ShipCarrierID = "GND"; //defaults for now, no other shipping option available
                    //salesTable.ShipCarrierAccountCode = "UPS"; //per the AX-UPS interface code sent from Britax
                    salesTable.ShipCarrierDlvType = AXWebService.AxdEnum_ShipCarrierDlvType.Ground;
                    salesTable.ShipCarrierDlvTypeSpecified = true;
                    salesTable.DeliveryDateControlType = AXWebService.AxdEnum_SalesDeliveryDateControlType.None;
                    salesTable.DeliveryDateControlTypeSpecified = true;

                    salesTable.TECShippingUnit = "ea";

                    foreach (OrderLine l in order.LineItems)
                    {

                        AXWebService.AxdEntity_SalesLine salesLine = new AXWebService.AxdEntity_SalesLine();
                        salesLine.ItemId = l.Sku;
                        salesLine.SalesQty = l.Quantity;
                        salesLine.SalesUnit = "ea";
                        //salesLine.SalesPrice = l.UnitPrice;
                        salesLine.SalesGroup = "";
                        salesLine.BRIEDIPrice = l.UnitPrice;
                        salesLine.BRIEDIPriceSpecified = true;
                        if (l.UnitPrice <= 0)
                        {
                            if (order.OrderType.ToLowerInvariant() == "r") //Recalls
                            {
                                salesLine.TECFOCReasonCode = "02";
                            }
                            else if (order.OrderType.ToLowerInvariant() == "a") //Advocates
                            {
                                salesLine.TECFOCReasonCode = "01";
                            }
                            else //Internet
                            {
                                salesLine.TECFOCReasonCode = "03";
                            }
                            //salesLine.BGTReasonCode = "FOC";
                        }

                        if (l.Discount > 0.0M)
                        {
                            salesLine.LineDisc = l.Discount;
                            salesLine.LineDiscSpecified = true;
                            salesLine.BGTReasonCode = "INET";
                        }
                        salesLines.Add(salesLine);
                    }

                    salesTable.MarkupTransHeader = new AXWebService.AxdEntity_MarkupTransHeader[]{
                    new AXWebService.AxdEntity_MarkupTransHeader(){
                        MarkupCode="Freight",
                        MarkupCategory=AXWebService.AxdExtType_MarkupCategoryType.Fixed,
                        Value=order.Shipping,
                        ValueSpecified=true,
                        Keep=AXWebService.AxdExtType_MarkupKeep.No,
                        KeepSpecified=true,
                        CurrencyCode=order.CurrencyCode
                    }
                };


                    // Add the sub-entity instances to their parent entities as an array
                    // of the sub-entity type.
                    salesTable.SalesLine = salesLines.ToArray();
                    salesOrder.SalesTable = new AXWebService.AxdEntity_SalesTable[1] { salesTable };

                    try
                    {

                        MessageHeader mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", endpoint);

                        OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
                        OperationContext.Current.OutgoingMessageHeaders.Add(mh);
                        //OperationContext.Current.OutgoingMessageHeaders.From = new EndpointAddress(new Uri("urn:TEST"));

                        AXWebService.EntityKey[] keys = client.create(salesOrder);

                        // The create method returns an EntityKey which contains the ID of the sales order.
                        statuses.Add(new OrderStatus() { OrderNumber = order.OrderNumber, AxOrderNumber = keys[0].KeyData[0].Value, Status = "Processed" });

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error processing order {0}: {1}", order.OrderNumber, ex.Message);
                        statuses.Add(new OrderStatus() { OrderNumber = order.OrderNumber, AxOrderNumber = "N/A", Status = "Failed: " + ex.Message });
                    }
                }
            

            return statuses.ToArray();
        }

        
        OrderStatus IAXProxy.TrackDownOrder(string orderType, string orderNumber, string paymentMode)
        {


            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            AXWebService.QueryCriteria qc = new AXWebService.QueryCriteria();
            AXWebService.CriteriaElement[] qe = { new AXWebService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record 
            AXWebService.EntityKey[] entityKey = { new AXWebService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "CustomerRef";
            qe[0].Operator = AXWebService.Operator.Equal;
            qe[0].Value1 = String.Format("{0}-{1} PT: {2}", orderType, orderNumber, paymentMode); //I-ORDER-16, e.g., followed by the credit card type;

            qc.CriteriaElement = qe;

            var client = SalesClient;
            string axOrderNumber = "";
            try
            {

                    AXWebService.AxdSalesOrder order = client.find(qc);

                    if (order != null && order.SalesTable!=null && order.SalesTable.Length > 0)
                    {
                        var table = order.SalesTable[0];
                        if (table.SalesStatus.HasValue)
                        {
                            axOrderNumber = table.SalesId;
                            status = table.SalesStatus.Value.ToString();
                        }
                        ship.Add(table.BEHDeliveryNum);
                        foreach (var line in table.SalesLine)
                        {
                            //if one line is delivered, mark the whole order delivered
                            if (line.SalesStatus == AXWebService.AxdEnum_SalesStatus.Delivered)
                                status = line.SalesStatus.ToString();
                            ship.Add(line.BEHDeliveryNum);
                        }

                    }
                
            }
            catch (Exception)
            {
            }
            return new OrderStatus()
            {
                AxOrderNumber = axOrderNumber,
                OrderNumber = orderNumber,
                Status = status,
                ShippingNumber = ship.Distinct()
            };
        }
    }
}
