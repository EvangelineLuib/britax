﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AXProxy
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAXProxy
    {

        [OperationContract]
        double GetTaxRate(string zipCode);

        [OperationContract]
        OrderStatus[] GetOrderStatuses(string[] orderNumbers);

        [OperationContract]
        OrderStatus GetOrderStatus(string orderNumber);

        [OperationContract]
        OrderStatus GetOrderByCustomer(string customerName);

        [OperationContract]
        InventoryLevel[] GetInventoryLevels(string[] skus);

        [OperationContract]
        InventoryLevel GetInventoryLevel(string sku);


        [OperationContract]
        OrderStatus[] SubmitOrders(OrderSubmission[] orders);


        [OperationContract]
        OrderStatus TrackDownOrder(string type, string ordernumber, string paymentMode);


    }


    [DataContract]
    public class InventoryLevel
    {
        string sku = "";
        decimal inventory = 0;

        [DataMember]
        public string Sku
        {
            get { return sku; }
            set { sku = value; }
        }

        [DataMember]
        public decimal Inventory
        {
            get { return inventory; }
            set { inventory = value; }
        }

    }
    [DataContract]
    public class OrderStatus
    {
        string status = "";
        string axOrderNumber = "";
        IEnumerable<string> shippingNumber = new List<string>();
        string orderNumber = "";

        [DataMember]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        [DataMember]
        public string OrderNumber
        {
            get { return orderNumber; }
            set { orderNumber = value; }
        }

        [DataMember]
        public string AxOrderNumber
        {
            get { return axOrderNumber; }
            set { axOrderNumber = value; }
        }


        [DataMember]
        public IEnumerable<string> ShippingNumber
        {
            get { return shippingNumber; }
            set { shippingNumber = value; }
        }
    }
    
    [DataContract]
    public class OrderLine
    {
        string sku = "";
        decimal quantity = 0;
        decimal unitPrice = 0.0M;
        decimal discount = 0.0M;
        string discountCode = "";

        [DataMember]
        public string Sku
        {
            get { return sku; }
            set { sku = value; }
        }

        [DataMember]
        public decimal Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }

        [DataMember]
        public decimal UnitPrice
        {
            get { return unitPrice; }
            set { unitPrice = value; }
        }

        [DataMember]
        public decimal Discount
        {
            get { return discount; }
            set { discount = value; }
        }

        [DataMember]
        public string DiscountCode
        {
            get { return discountCode; }
            set { discountCode = value; }
        }
    }

    [DataContract]
    public class OrderSubmission
    {
        string currencyCode = "USD";
        string account = "1101";
        string authorizationId = "";
        string orderNumber = "";
        string paymentMode = "";
        DateTime deliveryDate = DateTime.Now;
        List<OrderLine> lineItems = null;

        string name = "";
        string address = "";
        string street = "";
        string city = "";
        string state = "";
        string zip = "";
        string email = "";
        string orderType = "";
        string phone = "";
        decimal shipping = 0.0M;


        /// <summary>
        /// Gets or sets the order number.
        /// </summary>
        /// <value>
        /// The order number.
        /// </value>
        [DataMember]
        public string OrderNumber
        {
            get { return orderNumber; }
            set { orderNumber = value; }
        }
        [DataMember]
        public string CurrencyCode
        {
            get { return currencyCode; }
            set { currencyCode = value; }
        }

        [DataMember]
        public string AuthorizationId
        {
            get { return authorizationId; }
            set { authorizationId = value; }
        }

        [DataMember]
        public string Account
        {
            get { return account; }
            set { account = value; }
        }

        [DataMember]
        public string ShipName
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public string ShipAddress
        {
            get { return address; }
            set { address = value; }
        }

        [DataMember]
        public string ShipStreet
        {
            get { return street; }
            set { street = value; }
        }

        [DataMember]
        public string ShipCity
        {
            get { return city; }
            set { city = value; }
        }

        [DataMember]
        public string ShipState
        {
            get { return state; }
            set { state = value; }
        }

        [DataMember]
        public string ShipZip
        {
            get { return zip; }
            set { zip = value; }
        }
        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public DateTime ShipDate
        {
            get { return deliveryDate; }
            set { deliveryDate = value; }
        }

        [DataMember]
        public string ShipPhone
        {
            get { return phone; }
            set { phone = value; }
        }

        [DataMember]
        public string PaymentMode
        {
            get { return paymentMode; }
            set { paymentMode = value; }
        }

        [DataMember]
        public string OrderType
        {
            get { return orderType; }
            set { orderType = value; }
        }

        [DataMember]
        public decimal Shipping
        {
            get { return shipping; }
            set { shipping = value; }
        }

        [DataMember]
        public List<OrderLine> LineItems
        {
            get {
                if (lineItems == null) lineItems = new List<OrderLine>(); 
                return lineItems; 
            }
            set { lineItems = value; }
        }


    }
}
