﻿using AXWebServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AXProxyWS
{
    [ServiceContract]
    public interface IAXProxy
    {

        [OperationContract]
        double GetTaxRate(string zipCode);

        [OperationContract]
        OrderStatus[] GetOrderStatuses(string[] orderNumbers);

        [OperationContract]
        OrderStatus GetOrderStatus(string orderNumber);

        [OperationContract]
        OrderStatus GetOrderByCustomer(string customerName);

        [OperationContract]
        InventoryLevel[] GetInventoryLevels(string[] skus);

        [OperationContract]
        InventoryLevel GetInventoryLevel(string sku);


        [OperationContract]
        OrderStatus[] SubmitOrders(OrderSubmission[] orders);


        [OperationContract]
        OrderStatus TrackDownOrder(string type, string ordernumber, string paymentMode);
        [OperationContract]
        InventoryLevel[] GetInventoryLevelsFromTA(List<InventoryLevel> skus, string axAccountRelation, string axDataAreaId);

    }




 
}
