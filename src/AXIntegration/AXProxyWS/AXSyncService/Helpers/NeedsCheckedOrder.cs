﻿using AXSyncService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXSyncService
{
    class NeedsCheckedOrder
    {
        public TeaCommerce_Order Order { get; set; }
        public string OrderType { get; set; }
        public string PaymentMode { get; set; }
    }
}
