﻿using AXSyncService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXSyncService
{
    public static class ApiRequestHelper
    {
        public static AXApiRequest NewRequest(string action, UmbracoEntities db)
        {
            var request=new AXApiRequest()
            {
                RequestID=Guid.NewGuid(),
                Action=action,
                Used=false,
                Date=DateTime.Now
            };
         
           
            db.AXApiRequests.Add(request);
            db.SaveChanges();

            return request;
        }
    }
}
