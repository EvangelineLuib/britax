//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AXSyncService
{
    using System;
    using System.Collections.Generic;
    
    public partial class Registration
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
        public string phoneNumber { get; set; }
        public string emailAddress { get; set; }
        public string brand { get; set; }
        public string productType { get; set; }
        public string serialNumber { get; set; }
        public string modelNumber { get; set; }
        public string batchNumber { get; set; }
        public Nullable<System.DateTime> manufactureDate { get; set; }
        public string color { get; set; }
        public System.DateTime purchaseDate { get; set; }
        public string purchasedAt { get; set; }
        public Nullable<int> associatedUser { get; set; }
        public int associatedService { get; set; }
        public System.DateTime dateCreated { get; set; }
        public System.DateTime dateModified { get; set; }
        public string shippingStatus { get; set; }
        public Nullable<bool> registeredForRecall { get; set; }
        public string modelName { get; set; }
        public string actionCode { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public string StoreSuburb { get; set; }
        public string Proofofpurchase { get; set; }
        public Nullable<int> newsletter { get; set; }
    }
}
