//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AXSyncService
{
    using System;
    using System.Collections.Generic;
    
    public partial class TeaCommerce_Permission
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public Nullable<int> GeneralPermissionType { get; set; }
        public Nullable<int> StoreSpecificPermissionType { get; set; }
        public Nullable<long> StoreId { get; set; }
    
        public virtual TeaCommerce_Store TeaCommerce_Store { get; set; }
    }
}
