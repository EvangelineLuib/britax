﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXSyncService
{
 

    public enum eTCOrderStatus
    {
        
        Pending = 1,
        Backorder = 2,
        Cancelled = 3,
        Processed = 4,
        Shipped = 5,
        Error = 6

    }
}
