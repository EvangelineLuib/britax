//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AXSyncService
{
    using System;
    using System.Collections.Generic;
    
    public partial class TeaCommerce_PaymentMethod
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TeaCommerce_PaymentMethod()
        {
            this.TeaCommerce_Order = new HashSet<TeaCommerce_Order>();
            this.TeaCommerce_PaymentCurrency = new HashSet<TeaCommerce_PaymentCurrency>();
            this.TeaCommerce_PaymentMethodSetting = new HashSet<TeaCommerce_PaymentMethodSetting>();
            this.TeaCommerce_Region = new HashSet<TeaCommerce_Region>();
            this.TeaCommerce_Region1 = new HashSet<TeaCommerce_Region>();
        }
    
        public long Id { get; set; }
        public long StoreId { get; set; }
        public Nullable<long> VatGroupId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string ImageIdentifier { get; set; }
        public string PaymentProviderAlias { get; set; }
        public string Sku { get; set; }
        public bool AllowsGetStatus { get; set; }
        public bool AllowsCapturePayment { get; set; }
        public bool AllowsRefundPayment { get; set; }
        public bool AllowsCancelPayment { get; set; }
        public int Sort { get; set; }
        public bool IsDeleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeaCommerce_Order> TeaCommerce_Order { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeaCommerce_PaymentCurrency> TeaCommerce_PaymentCurrency { get; set; }
        public virtual TeaCommerce_Store TeaCommerce_Store { get; set; }
        public virtual TeaCommerce_VatGroup TeaCommerce_VatGroup { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeaCommerce_PaymentMethodSetting> TeaCommerce_PaymentMethodSetting { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeaCommerce_Region> TeaCommerce_Region { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeaCommerce_Region> TeaCommerce_Region1 { get; set; }
    }
}
