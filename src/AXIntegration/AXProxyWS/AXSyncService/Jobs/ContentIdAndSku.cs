﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AXSyncService
{
    class ContentIdAndSku
    {
        public int ContentId
        {
            get;
            set;
        }
        public string Sku
        {
            get;
            set;
        }
    }
}
