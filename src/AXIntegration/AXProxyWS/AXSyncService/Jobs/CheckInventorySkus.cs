﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Configuration;
using System.Threading.Tasks;
using AXSyncService.WS_AXProxy;

namespace AXSyncService
{


    using Britax.Common.Models;

    public sealed class CheckInventorySkus
	{
		private static readonly Lazy<CheckInventorySkus> lazy =
		 new Lazy<CheckInventorySkus>(() => new CheckInventorySkus());

		public static CheckInventorySkus Instance { get { return lazy.Value; } }


		public void Execute()
		{
			var client = new AXProxyClient();
			List<SkuWithName> skuListFromAx = new List<SkuWithName>();

			int batchNumber = 0, batchSize = 500, maxBatchCount = 20; //expected total rows is 5998 as of Feb 23, 2017
			SkuWithName[] skuListBatch = client.GetInventorySkus(batchNumber, batchSize);
			while (skuListBatch.Length > 0 && batchNumber < maxBatchCount)
			{
				skuListFromAx = skuListFromAx.Concat(skuListBatch).ToList();
				batchNumber += 1;
				skuListBatch = client.GetInventorySkus(batchNumber, batchSize);
			}
			client.Close();

			var skuListFromAxBatch = skuListFromAx.Select((i, idx) => new { Item = i, Index = idx })
			   .GroupBy(x => x.Index / 5)
			   .Select(g => g.Select(x => x.Item).ToList())
			   .ToList();

			var db = new UmbracoEntities();
			foreach (var batchItem in skuListFromAxBatch)
			{
				this.SetSkusInUmbraco(batchItem.ToArray(), db);
			}

			var jobLog = new AXJobLog()
			{
				Job = "AX Inventory Skus",
				Status = "Success",
				Date = DateTime.Now
			};

			db.AXJobLogs.Add(jobLog);
            db.Database.ExecuteSqlCommand("DELETE FROM AXJobLog WHERE Date <= DATEADD(dd, -30, GETDATE())");
			db.SaveChanges();
		}

		private void SetSkusInUmbraco(SkuWithName[] skuListFromAx, UmbracoEntities db)
		{
			Task.Factory.StartNew(() =>
			{
				var request = new AXApiRequest()
				{
					RequestID = Guid.NewGuid(),
					Action = "SetModelNumbers",
					Used = false,
					Date = DateTime.Now
				};

				db.AXApiRequests.Add(request);
				db.SaveChanges();

				UpdateViaApi(skuListFromAx, request);

			}).Wait();
		}

		private static void UpdateViaApi(SkuWithName[] skuListFromAx, AXApiRequest request)
		{
			var client = new HttpClient();
			var apiEnd = $"Umbraco/Api/AXEntryApi/SetModelNumbers?request={request.RequestID}";
			var baseUrl = ConfigurationManager.AppSettings["URLWebAPI"]?.ToString();
			client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
			if (baseUrl != null) client.BaseAddress = new Uri(baseUrl);

			var result = client.PostAsJsonAsync(apiEnd, skuListFromAx).Result;


		}

		private void RecursivelyLogException(Exception ex)
		{
			//  Log.Debug(String.Format("[CheckInventoryJob] Error :{0}\r\n{1}", ex.Message, ex.StackTrace));
			if (ex.InnerException != null)
				RecursivelyLogException(ex.InnerException);

			if (ex is AggregateException)
			{
				AggregateException aex = (AggregateException)ex;
				foreach (Exception iex in aex.InnerExceptions)
					RecursivelyLogException(iex);

			}

		}
	}
}