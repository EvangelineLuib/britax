﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Threading;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using AXSyncService;
using AXSyncService.WS_AXProxy;
using System.Collections;
using Hangfire;
using Hangfire.Logging;
using Serilog;


namespace AXSyncService
{
    using System.Reflection;

    using Autofac;

    using AXIOC;


    using Britax.Common.Models;
    using Newtonsoft.Json;

    //using OrderStatus = AXSyncService.WS_AXProxy.OrderStatus;

    public sealed class  PushToAXJob
    {

        private static readonly Lazy<PushToAXJob> lazy =
        new Lazy<PushToAXJob>(() => new PushToAXJob());

        public static PushToAXJob Instance { get { return lazy.Value; } }

        public delegate void OnOrderStatusUpdatedEventHandler(Guid orderId);

        public static event OnOrderStatusUpdatedEventHandler OnOrderStatusUpdated;

        public delegate void OnOrderErrorEventHandler(Guid orderId, string errorMessage);

        public static event OnOrderErrorEventHandler OnOrderError;

        private string GetOrderProperty(UmbracoEntities db, TeaCommerce_Order order, string alias, string defaultValue)
        {
            var prop = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(y => y.Alias.Equals(alias) && y.TeaCommerce_Order.Id==order.Id);
            if (prop != null) return prop.Value;

            return defaultValue;

        }

        private int ResolveOrderStatus(string statusFromAx)
        {

            eTCOrderStatus statusType;

            if (Enum.TryParse(statusFromAx, out statusType))
                return (int)Enum.Parse(typeof(eTCOrderStatus), statusFromAx);
            else
            {
                switch (statusFromAx)
                {
                    case "Delivered":
                        return (int)eTCOrderStatus.Shipped;

                    case "Invoiced":
                        return (int)eTCOrderStatus.Shipped;

                    case "None":
                        return (int)eTCOrderStatus.Processed;

                }

                return (int)eTCOrderStatus.Error;
            }
        }
        public void Execute()
        {
               var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
            defaultLogger.Information("PUSH TO AX");



            try
            {

                defaultLogger.Information("this.ProcessNewOrders();");
                this.ProcessNewOrders();
            }
            catch (Exception exx)
            {
                defaultLogger.Error(exx, "ERROR ProcessNewOrders");
            }


            try
            {
                defaultLogger.Information("   this.ProcessOrdersStatus();");
                this.ProcessOrdersStatus();
            }
            catch (Exception exx)
            {
                defaultLogger.Error(exx, "ERROR ProcessOrdersStatus");
            }
        }
        private void ProcessOrdersStatus()
        {
            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
            defaultLogger.Information("PUSH TO AX ProcessOrdersStatus");

            using (var db = new UmbracoEntities())
            {
                var toSubmit = new List<OrderSubmission>();
                var statuses = new OrderStatus[0];

                var orderForCheck =
                    db.TeaCommerce_Order.Where(o => (o.OrderStatusId == 4) && o.DateFinalized != null
                                            && (!string.IsNullOrEmpty(o.TransactionId) && o.TransactionId != "0")
                                    && o.OrderNumber != "")
                        .Include(y => y.TeaCommerce_CustomOrderProperty)
                        .ToList();

                if (!orderForCheck.Any())
                {
                    defaultLogger.Information($"ProcessOrdersStatus  NO RECORD");
                    return;
                }


                var proxyClient = new AXSyncService.WS_AXProxy.AXProxyClient();


                List<OrderStatus> orderStatuslist = new List<OrderStatus>();
                foreach (var lineOrder in orderForCheck)
                {

                    var axReference =
                        lineOrder.TeaCommerce_CustomOrderProperty.FirstOrDefault(e => e.Alias == "AXOrderNumber");

                    if (axReference != null)
                    {
                        var newOrder = new OrderStatus
                        {
                            OrderId = lineOrder.Id,
                            AxOrderNumber = axReference.Value ,
                            OrderNumber = lineOrder.OrderNumber

                        };
                        orderStatuslist.Add(newOrder);
                    }

                }

                var orderForCheckBatch = orderStatuslist.Select(
                              (i, idx) => new { Item = i, Index = idx })
                         .GroupBy(x => x.Index / 5)
                         .Select(g => g.Select(x => x.Item).ToList())
                         .ToList();




                foreach (var bathOrders in orderForCheckBatch)
                {
                    var returnStatus = proxyClient.GetOrderStatusByList(bathOrders.ToArray());

                    if (returnStatus.Any())
                    {
                        var serializestatuses = JsonConvert.SerializeObject(returnStatus);
                        defaultLogger.Information($" RETURN to Umbraco proxyClient.ProcessOrdersStatus = > {serializestatuses}");

                          var success = this.UpdateOrderStatuses(returnStatus.ToArray(), false, db);
                    }

                }


            }
        }

        private void ProcessNewOrders()
        {
            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();

            using (var db = new UmbracoEntities())
            {
                var toSubmit = new List<OrderSubmission>();
                var statuses = new OrderStatus[0];

                var finalizedOrders = db.TeaCommerce_Order.Where(o =>
                        o.OrderStatusId == 1 &&
                        o.DateFinalized != null &&
                        (!string.IsNullOrEmpty(o.TransactionId) && o.TransactionId != "0"));
                var orders = finalizedOrders
                    .Include(y => y.TeaCommerce_CustomOrderProperty)
                    .Include(y => y.TeaCommerce_OrderLine.Select(c => c.TeaCommerce_CustomOrderLineProperty))
                    .Include(y => y.TeaCommerce_Currency)
                    .ToList();

                orders = orders.Where(e => this.ShouldSubmitOrder(e.Id, e.OrderNumber)).ToList();

                if (!orders.Any())
                {
                    defaultLogger.Information("TEA COMMERCE ORDER NOT FOUND");
                    return;
                }
                else
                {
                    var icount = orders.Count;
                    defaultLogger.Information($"TEA COMMERCE   ORDER FOR PROCESS = > {icount} ") ;
                }
                var proxyClient = new AXSyncService.WS_AXProxy.AXProxyClient();

                defaultLogger.Information($"[PushToAxJob] Processing {orders.Count()} orders");

                foreach (var order in orders)
                {
                    var properties = order.TeaCommerce_CustomOrderProperty;
                    var lines = order.TeaCommerce_OrderLine;
                    var currency = order.TeaCommerce_Currency.Name;

                    var shipAddress = properties.FirstOrDefault(x => x.Alias == "shipping_streetAddress")?? new TeaCommerce_CustomOrderProperty();
                    var shipCity = properties.FirstOrDefault(x => x.Alias == "shipping_city") ?? new TeaCommerce_CustomOrderProperty();
                    var shipState = properties.FirstOrDefault(x => x.Alias == "shipping_stateOrProvince") ?? new TeaCommerce_CustomOrderProperty();
                    var shipZip = properties.FirstOrDefault(x => x.Alias == "shipping_zipCode") ?? new TeaCommerce_CustomOrderProperty();
                    var shipFirstName = properties.FirstOrDefault(x => x.Alias == "shipping_firstName") ?? new TeaCommerce_CustomOrderProperty();
                    var shipLirstName = properties.FirstOrDefault(x => x.Alias == "shipping_lastName") ?? new TeaCommerce_CustomOrderProperty();
                    var shipPhone = properties.FirstOrDefault(x => x.Alias == "shipping_phone") ?? new TeaCommerce_CustomOrderProperty();
                    var email = properties.FirstOrDefault(x => x.Alias == "confirmation_email") ?? new TeaCommerce_CustomOrderProperty() { Value = "" };
                    var cardType = properties.FirstOrDefault(x => x.Alias == "cardType") ?? new TeaCommerce_CustomOrderProperty() { Value = "" };

                    var unattendedDelivery = (properties.FirstOrDefault(x => x.Alias == "unattendedDelivery") ?? new TeaCommerce_CustomOrderProperty() { Value = "" }).Value;

                    var globalDiscount = (properties.FirstOrDefault(x => x.Alias == "GlobalDiscount") ?? new TeaCommerce_CustomOrderProperty() { Value = "" }).Value;
                    var isGlobalDiscount = false;
                    bool.TryParse(globalDiscount, out isGlobalDiscount);

                    var discountAmount = (properties.FirstOrDefault(x => x.Alias == "DiscountAmount") ?? new TeaCommerce_CustomOrderProperty());
                    var discountCode = (properties.FirstOrDefault(x => x.Alias == "DiscountCodeClient") ?? new TeaCommerce_CustomOrderProperty());
                    var shipping = properties.FirstOrDefault(x => x.Alias == "ShippingTotal") ?? new TeaCommerce_CustomOrderProperty();
                    var discount = 0.0M;
                    decimal.TryParse(discountAmount.Value, out discount);

                    var shippingCost = 0.0M;
                    decimal.TryParse(shipping.Value, out shippingCost);

                    var suffix = "NET03";
                    string orderType = "I";
                    string account = "WEBTEMPLATE";

                    var submittedLines = new List<OrderLine>();
                    int count = lines.Count();

                    foreach (var ol in lines)
                    {
                        var lineProps = ol.TeaCommerce_CustomOrderLineProperty;
                        var upc = lineProps.FirstOrDefault(x => x.Alias == "upc")
                                  ?? new TeaCommerce_CustomOrderLineProperty() { Value = "__none__" };
                        var lineDiscountAmountProp = lineProps.FirstOrDefault(x => x.Alias == "DiscountAmount")
                                                     ?? new TeaCommerce_CustomOrderLineProperty() { Value = "__none__" };
                        decimal lineDiscountAmount;
                        var disc = 0.0M;
                        var code = "";
                        if (lineDiscountAmountProp.Value != "__none__"
                            && decimal.TryParse(lineDiscountAmountProp.Value, out lineDiscountAmount))
                        {
                            disc = lineDiscountAmount / ol.Quantity;
                        }

                        var sku =
                            (lineProps.FirstOrDefault(x => x.Alias == "modelNumber")
                             ?? new TeaCommerce_CustomOrderLineProperty()).Value;

                        if (!string.IsNullOrEmpty(sku))
                        {
                            submittedLines.Add(new OrderLine()
                            {
                                Quantity = ol.Quantity,
                                Sku = sku,
                                UnitPrice = ol.UnitPriceWithoutDiscounts,
                                Discount = decimal.Round(disc, 2, MidpointRounding.AwayFromZero),
                                DiscountCode = code
                            });
                        }
                    }

                    if (submittedLines.Any())
                    {
                        var newOrder = new OrderSubmission
                        {
                            PaymentMode = "Paypal",
                            Account = account,
                            AuthorizationId = order.TransactionId,
                            CurrencyCode = "AUD",
                            OrderNumber = order.OrderNumber,
                            OrderId = order.Id,
                            Shipping = order.ShippingTotalPriceWithoutDiscounts,
                            ShipPhone = shipPhone.Value,
                            ShipName = $"{shipFirstName.Value} {shipLirstName.Value}",
                            ShipStreet = shipAddress.Value,
                            ShipZip = shipZip.Value,
                            ShipCity = shipCity.Value,
                            ShipState = shipState.Value,
                            ShipDate = DateTime.Now,
                            UnattendedDelivery = unattendedDelivery,
                            Email = email.Value,
                            OrderType = orderType,
                            LineItems = submittedLines,
                            SalesDiscount = discount
                        };

                        var serializeOrder = JsonConvert.SerializeObject(newOrder);

                        defaultLogger.Information($"TEA COMMERCE NEW ORDER = > {serializeOrder}");
                        toSubmit.Add(newOrder);
                    }
                }

                var PAGE_SIZE = 10;

                var today = DateTime.Now;
                toSubmit.ForEach(order => this.MarkAsSubmitted(order.OrderId, order.OrderNumber, today));

                var batches = new List<List<OrderSubmission>>();
                toSubmit.ForEach(order =>
                {
                    if (batches.LastOrDefault() == null || batches.LastOrDefault().Count >= PAGE_SIZE)
                    {
                        batches.Add(new List<OrderSubmission>());
                    }

                    batches.LastOrDefault().Add(order);
                });

                batches.ForEach(batch =>
                {
                    var serializeOrderArray = JsonConvert.SerializeObject(batch);
                    defaultLogger.Information($"SUBMIT TO AX= > {serializeOrderArray}");

                    try
                    {
                        statuses = proxyClient.SubmitOrders(batch.ToArray());
                        if (statuses != null)
                        {
                            var serializestatuses = JsonConvert.SerializeObject(statuses);
                            defaultLogger.Information($" RETURN to Umbraco proxyClient.SubmitOrders(batch.ToArray()); = > {serializestatuses}");
                            var submissionResponseDate = DateTime.Now;
                            foreach (var statusLine in statuses)
                            {
                                this.UpdateOrdersWithAxOrderNumber( db, statusLine, statusLine.OrderId, "AXOrderNumber", statusLine.AxOrderNumber, false, false);
                                this.UpdateOrdersWithAxOrderNumber(db, statusLine, statusLine.OrderId, "LastAXLog", statusLine.Status);

                                this.MarkSubmittedAsSynced(statusLine.OrderId, statusLine.OrderNumber, submissionResponseDate);
                            }
                        }
                        else
                        {
                            defaultLogger.Information($" RETURN to Umbraco proxyClient.SubmitOrders(batch.ToArray()); = >EMPTY /NULL");
                        }
                    }
                    catch (Exception e)
                    {
                        defaultLogger.Error(e,$"ERROR SUBMIT TO AX= > {serializeOrderArray}");
                        defaultLogger.Error(e, "proxyClient.SubmitOrder");

                        var failedOrderInfos = batch.Select(x => new FailedOrderInfo
                        {
                            OrderType = x.OrderType,
                            OrderNumber = x.OrderNumber,
                            PaymentMode = x.PaymentMode
                        });

                        if (!this.MarkOrdersAsNeedRechecked(failedOrderInfos, db))
                        {
                            // Log.Debug(String.Format("[PushToAxJob] There was a problem submitting orders from batch count {0}, and then there was an error trying to mark them as needing rechecked again.", batchcount));
                        }

                        foreach (var statusInfo in statuses.Where(statusInfo => statusInfo.Status.StartsWith("Failed: ")))
                        {
                            OnOrderError?.Invoke(statusInfo.OrderId, e.GetBaseException().Message);
                        }
                    }

                    var success = this.UpdateOrderStatuses(statuses, false, db);
                    //  Log.Debug(String.Format("[PushToAxJob] Sending {0} orders to Umbraco to flag statuses", statuses.Length));
                });

                var jobLog = new AXJobLog() { Job = "AX Order Submission", Status = "Success", Date = DateTime.Now };

                db.AXJobLogs.Add(jobLog);
                db.Database.ExecuteSqlCommand("DELETE FROM AXJobLog WHERE Date <= DATEADD(dd, -30, GETDATE())");
                db.SaveChanges();
            }
        }

        private bool ShouldSubmitOrder(Guid orderId, string orderNumber)
        {
            using (var db = new UmbracoEntities())
            {
                var dateSubmitted = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e =>
                    e.OrderId == orderId &&
                    e.TeaCommerce_Order.OrderNumber == orderNumber &&
                    e.Alias == "DateSubmittedToAx"
                );

                var dateSynced = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e =>
                    e.OrderId == orderId &&
                    e.TeaCommerce_Order.OrderNumber == orderNumber &&
                    e.Alias == "DateSyncedToAx"
                );

                var axOrderNumber = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e =>
                    e.OrderId == orderId &&
                    e.TeaCommerce_Order.OrderNumber == orderNumber &&
                    e.Alias == "AXOrderNumber"
                );

                var notYetSubmitted = string.IsNullOrEmpty(dateSubmitted?.Value);
                if (notYetSubmitted)
                {
                    return true;
                }

                var oneHourSinceLastSubmission = DateTime.Parse(dateSubmitted.Value).AddHours(1) < DateTime.Now;
                var notYetSynced = string.IsNullOrEmpty(axOrderNumber?.Value) || dateSynced?.Value == null;

                return oneHourSinceLastSubmission && notYetSynced;
            }
        }

        private void MarkAsSubmitted(Guid orderId, string orderNumber, DateTime dateSubmitted)
        {
            using (var db = new UmbracoEntities())
            {
                var dateSubmittedProperty = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e =>
                    e.OrderId == orderId &&
                    e.TeaCommerce_Order.OrderNumber == orderNumber &&
                    e.Alias == "DateSubmittedToAx"
                );

                if (dateSubmittedProperty == null)
                {
                    dateSubmittedProperty = new TeaCommerce_CustomOrderProperty
                    {
                        OrderId = orderId,
                        Alias = "DateSubmittedToAx",
                        Value = dateSubmitted.ToString()
                    };
                    db.Entry<TeaCommerce_CustomOrderProperty>(dateSubmittedProperty).State = EntityState.Added;
                }
                else
                {
                    dateSubmittedProperty.Value = dateSubmitted.ToString();
                    db.Entry<TeaCommerce_CustomOrderProperty>(dateSubmittedProperty).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        private void MarkSubmittedAsSynced(Guid orderId, string orderNumber, DateTime dateSynced)
        {
            using (var db = new UmbracoEntities())
            {
                var dateSyncedProperty = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e =>
                    e.OrderId == orderId &&
                    e.TeaCommerce_Order.OrderNumber == orderNumber &&
                    e.Alias == "DateSyncedToAx"
                );

                if (dateSyncedProperty == null)
                {
                    dateSyncedProperty = new TeaCommerce_CustomOrderProperty
                    {
                        OrderId = orderId,
                        Alias = "DateSyncedToAx",
                        Value = dateSynced.ToString()
                    };
                    db.Entry<TeaCommerce_CustomOrderProperty>(dateSyncedProperty).State = EntityState.Added;
                }
                else
                {
                    dateSyncedProperty.Value = dateSynced.ToString();
                    db.Entry<TeaCommerce_CustomOrderProperty>(dateSyncedProperty).State = EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        private IEnumerable<TeaCommerce_Order> RecheckOrdersAndUpdateIfNeeded(IEnumerable<NeedsCheckedOrder> orders, AXProxyClient ax, UmbracoEntities db)
        {
            //The orders that were found in AX and need updated in our system.
            List<OrderStatus> ordersThatNeedUpdated = new List<OrderStatus>();

            //Orders that were NOT found in ax and that need to be ran like normal.
            List<TeaCommerce_Order> ordersThatWerentFound = new List<TeaCommerce_Order>();

            foreach(NeedsCheckedOrder order in orders)
            {
                OrderStatus orderStatus = ax.TrackDownOrder(order.OrderType, order.Order.OrderNumber, order.PaymentMode);

                if(orderStatus.Status != "Not Found") //If the status is NOT "Not Found" then we know it was found.
                {
                    ordersThatNeedUpdated.Add(orderStatus);
                }
                else
                {
                    ordersThatWerentFound.Add(order.Order);
                }
            }

            //Now update the ax orders
            if(ordersThatNeedUpdated.Any())
            {
                bool anyFailures = false;
                OrderStatus[] orderStatusesArray = ordersThatNeedUpdated.ToArray();

                //Update the AX Number on the orders.
                List<string> failedOrders = UpdateOrdersWithAxOrderNumber(orderStatusesArray, db);

                if(failedOrders.Any())
                {
                    anyFailures = true;
                 //   Log.Debug(String.Format("Could not update Orders with AX number after 'Tracking them down'."));
                }

                //Update the order statuses of the orders in TeaCommerce
                bool status = UpdateOrderStatuses(orderStatusesArray, false,db);
                if(!status)
                {
                    anyFailures = true;
                   // Log.Debug(String.Format("Could not updated order status after 'Tracking them down'."));
                }

                if(!anyFailures)
                {
                    //Everything should be sorted out correctly in our system. Remove the property that flags this as needing checked so its not picked up next time.
                    bool markedSuccessfully = MarkOrdersAsNotNeededChecked(ordersThatNeedUpdated.Select(x => x.OrderNumber), db);

                    if(!markedSuccessfully)
                    {
                      //  Log.Debug("Encountered an error trying to mark the orders as not needing checked anymore.");
                    }
                }
            }

            //now return a list of orders that weren't found
            return ordersThatWerentFound;
        }






  private bool MarkOrdersAsNotNeededChecked(IEnumerable<string> orderNumbers, UmbracoEntities db)
        {
            bool anyErrors = false;

            foreach(string orderNum in orderNumbers)
            {
                try
                {

                    var prop1 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "NeedsCheckedWithAX");
                    var prop2 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "OrderType");
                    var prop3 = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNum && p.Alias == "PaymentMode");

                    if (prop1 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop1);
                        db.SaveChanges();
                    }
                    if (prop2 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop2);
                        db.SaveChanges();
                    }
                    if (prop3 != null)
                    {
                        db.TeaCommerce_CustomOrderProperty.Remove(prop3);
                        db.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    anyErrors = true;
                  //  Log.Debug(String.Format("Could not deleted order property NeedsCheckedWithAX for order number {0}", orderNum), e);
                }
            }
            return anyErrors;
        }



          private bool MarkOrdersAsNeedRechecked(IEnumerable<FailedOrderInfo> failedOrders, UmbracoEntities db)
        {
            bool anyErrors = false;

            foreach(var failedOrder in failedOrders)
            {
                try
                {
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "NeedsCheckedWithAX", "1");
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "OrderType", failedOrder.OrderType);
                    AddOrUpdateOrderProperty(db, failedOrder.OrderNumber, "PaymentMode", failedOrder.PaymentMode);
                }
                catch (Exception e)
                {
                    anyErrors = true;
                    //Log.Debug(String.Format("Could not update order with order number {0}", failedOrder.OrderNumber), e);
                }
            }
            return !anyErrors;
        }



        //string orderNumber, string alias, string value

        private  void UpdateOrdersWithAxOrderNumber(UmbracoEntities db ,OrderStatus status, Guid orderId, string alias , string aliasValue, bool serverSideOnly = true, bool isReadOnly = true)
        {

            var prop =db.TeaCommerce_CustomOrderProperty.FirstOrDefault(e => e.OrderId == orderId  && e.Alias == alias);

            if (prop != null)
            {
                prop.Value = aliasValue;
                db.Entry(prop).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                prop = new TeaCommerce_CustomOrderProperty()
                {
                    OrderId = orderId,
                    Alias = alias,
                    Value = aliasValue,
                    IsReadOnly = isReadOnly,
                    ServerSideOnly = serverSideOnly
                };
                db.TeaCommerce_CustomOrderProperty.Add(prop);
                db.SaveChanges();

            }

        }


        private List<string> UpdateOrdersWithAxOrderNumber(OrderStatus[] statuses, UmbracoEntities db)
        {

            List<string> failedOrders = new List<string>();


            foreach (var status in statuses)
            {
                try
                {
                    if (status.AxOrderNumber == "N/A")
                    {
                        //     Log.Debug(String.Format("Order failed during submission: {0}, {1}", status.OrderNumber, status.Status));
                        this.AddOrUpdateOrderProperty(db, status.OrderNumber, "LastAXLog", status.Status);
                    }
                    else
                    {
                        this.AddOrUpdateOrderProperty(db, status.OrderNumber, "AXOrderNumber", status.AxOrderNumber, false, false);
                        this.AddOrUpdateOrderProperty(db, status.OrderNumber, "LastAXLog", status.Status);
                    }
                }
                catch (Exception e)
                {
                    failedOrders.Add(status.OrderNumber);
                 //   Log.Debug(String.Format("Could not update order with order number {0} and AX Order number {1}", status.OrderNumber, status.AxOrderNumber), e);
                }

            }

            return failedOrders;
        }

        private void AddOrUpdateOrderProperty(UmbracoEntities db, string orderNumber, string alias, string value, bool serversideOnly = true, bool isReadOnly = true)
        {

            var prop = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNumber && p.Alias == alias);
            if (prop != null)
            {
                prop.Value = value;
                db.Entry(prop).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                prop = new TeaCommerce_CustomOrderProperty()
                {
                    OrderId = db.TeaCommerce_Order.First(o => o.OrderNumber == orderNumber).Id,
                    Alias = alias,
                    Value = value,
                    IsReadOnly = isReadOnly,
                    ServerSideOnly = serversideOnly
                };
                db.TeaCommerce_CustomOrderProperty.Add(prop);
                db.SaveChanges();
            }
        }


        /// <summary>
        /// Makes a call to the AXEntryApi/UpdateOrderStatuses, which will update the order status of the TeaCommerce Order.
        /// </summary>
        /// <param name="statuses">The order statuses from AX</param>
        /// <param name="orderNumberIsAxOrderNumber">Need this in here because the get order statuses in the AX proxy returns the Ax Order Number in the Ordernumber field</param>
        /// <returns>True if there were no errors on the API side. False, if there were errors (not necessarily fatal)</returns>
        private bool UpdateOrderStatuses(OrderStatus[] statuses, bool orderNumberIsAxOrderNumber , UmbracoEntities db)
        {
            if (statuses == null) throw new ArgumentNullException(nameof(statuses));


            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
            defaultLogger.Information("UpdateOrderStatuses ");


            foreach (var status in statuses)
            {
                var tcStatusId = this.ResolveOrderStatus(status.Status);
                defaultLogger.Information($"LookUp {status.Status} returns {tcStatusId}");
                try
                {
                    var order =db.TeaCommerce_Order.FirstOrDefault(o => o.Id == status.OrderId);

                    if (order != null)
                    {
                        order.OrderStatusId = tcStatusId;
                        defaultLogger.Information($"UpdateOrderStatuses status.OrderNumber {status.OrderNumber} order.OrderStatusId {order.OrderStatusId}");
                        db.SaveChanges();
                        OnOrderStatusUpdated?.Invoke(order.Id);
                    }
                    else
                    {
                        defaultLogger.Information($"UpdateOrderStatuses status.OrderNumber {status.OrderNumber} not found ");
                    }
                }
                catch (Exception e)
                {
                    defaultLogger.Error(e, "UpdateOrderStatuses");
                }
            }
            return true;
        }

    }

    public class FailedOrderInfo
    {
        public string OrderType { get; set; }
        public string OrderNumber { get; set; }
        public string PaymentMode { get; set; }
    }
}

