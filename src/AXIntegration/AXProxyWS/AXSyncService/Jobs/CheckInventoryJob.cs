﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

using System.Configuration;
using System.Threading.Tasks;
using AXSyncService.WS_AXProxy;

namespace AXSyncService
{
    using Autofac;

    using AXIOC;

    using Britax.Common.Models;

    using Newtonsoft.Json;

    using Serilog;

    public sealed class CheckInventoryJob
    {
        private static readonly Lazy<CheckInventoryJob> lazy =
         new Lazy<CheckInventoryJob>(() => new CheckInventoryJob());

        public static CheckInventoryJob Instance { get { return lazy.Value; } }


        public void Execute()
        {
            return;

            var db = new UmbracoEntities();


            //var query = @"SELECT  cmsPropertyData.dataNvarchar as Sku, cast(0 as decimal) as  Inventory, cast( C.nodeId as varchar(20)) as UmbracoNode   , cast(0 as decimal) as  Price  FROM cmsContent C INNER JOIN cmsContentType CT ON C.contentType = CT.nodeId INNER JOIN umbracoNode N ON C.nodeId = N.id		inner join cmsContentXml X ON C.nodeId=x.nodeId 	left join umbracoNode ParentNode on N.parentID= ParentNode.id	inner join cmsDocument on cmsDocument.nodeId= N.id	inner join cmsPropertyData  on cmsPropertyData.versionId= cmsDocument.versionId	inner join cmsPropertyType on cmsPropertyData .propertytypeid = cmsPropertyType.id	WHERE CT.alias ='ProductVariant' and 			cmsDocument.newest=1 and		cmsPropertyType.Alias='modelNumber' and	cmsPropertyData.dataNvarchar is not null and	cmsPropertyData.dataNvarchar  not like '000%'	order by 1";
            var query = @"SELECT  cast(cmsPropertyData.dataNtext as varchar(max)) as Sku, cast(0 as decimal) as  Inventory, cast( C.nodeId as varchar(20)) as UmbracoNode   , cast(0 as decimal) as  Price  FROM cmsContent C INNER JOIN cmsContentType CT ON C.contentType = CT.nodeId INNER JOIN umbracoNode N ON C.nodeId = N.id		inner join cmsContentXml X ON C.nodeId=x.nodeId 	left join umbracoNode ParentNode on N.parentID= ParentNode.id	inner join cmsDocument on cmsDocument.nodeId= N.id	inner join cmsPropertyData  on cmsPropertyData.versionId= cmsDocument.versionId	inner join cmsPropertyType on cmsPropertyData .propertytypeid = cmsPropertyType.id	WHERE CT.alias ='ProductVariant' and 			cmsDocument.newest=1 and		cmsPropertyType.Alias='modelNumber' and	 cast(cmsPropertyData.dataNtext as varchar(max)) is not null	order by 1";


            var listItems = db.Database.SqlQuery<InventoryLevel>(query).ToList();


            var client = new AXProxyClient();

            var pricingGroup = ConfigurationManager.AppSettings.Get("PricingGroup");
            if (string.IsNullOrEmpty(pricingGroup))
            {
                pricingGroup = "B2C";
            }

            var skuListFromAx = client.GetInventoryLevelsFromTA(listItems.ToArray(), pricingGroup, "btx");



            var skuListFromAxBatch = skuListFromAx.Select((i, idx) => new { Item = i, Index = idx })
               .GroupBy(x => x.Index / 5)
               .Select(g => g.Select(x => x.Item).ToList())
               .ToList();

            foreach (var bathItem in skuListFromAxBatch)
            {
                this.SetInventoryLevelsInUmbraco(bathItem.ToArray(), db);
            }




            var jobLog = new AXJobLog()
            {
                Job = "AX Inventory Levels",
                Status = "Success",
                Date = DateTime.Now
            };

            db.AXJobLogs.Add(jobLog);
            db.Database.ExecuteSqlCommand("DELETE FROM AXJobLog WHERE Date <= DATEADD(dd, -30, GETDATE())");
            db.SaveChanges();




        }

        private void SetInventoryLevelsInUmbraco(InventoryLevel[] skuListFromAx, UmbracoEntities db)
        {


            Task.Factory.StartNew(() =>
            {
                var request = new AXApiRequest()
                {
                    RequestID = Guid.NewGuid(),
                    Action = "SetInventory",
                    Used = false,
                    Date = DateTime.Now
                };

                db.AXApiRequests.Add(request);
                db.SaveChanges();

                UpdateViaApi(skuListFromAx, request);

            }).Wait();


}

        private static void UpdateViaApi(InventoryLevel[] skuListFromAx, AXApiRequest request)
        {
            try
            {
                var client = new HttpClient();
                var apiEnd = $"Umbraco/Api/AXEntryApi/SetInventory?request={request.RequestID}";
                var baseUrl = ConfigurationManager.AppSettings["URLWebAPI"]?.ToString();
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                if (baseUrl != null) client.BaseAddress = new Uri(baseUrl);

                var result = client.PostAsJsonAsync(apiEnd, skuListFromAx).Result;
            }
            catch (Exception xx)
            {
                var defaultLogger2 = Bootstrapper.Container.Resolve<ILogger>();

                defaultLogger2.Error(xx, "UpdateViaApi");

            }


        }

        private void RecursivelyLogException(Exception ex)
        {
          //  Log.Debug(String.Format("[CheckInventoryJob] Error :{0}\r\n{1}", ex.Message, ex.StackTrace));
            if (ex.InnerException != null)
                RecursivelyLogException(ex.InnerException);

            if(ex is AggregateException)
            {
                AggregateException aex = (AggregateException)ex;
                foreach (Exception iex in aex.InnerExceptions)
                    RecursivelyLogException(iex);

            }

        }
    }
}