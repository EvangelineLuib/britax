﻿ 
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

using System.Net;
using System.Data.Entity;

using AXSyncService;


namespace AXSyncService
{
    using Britax.Common.Models;

    public class UpdateStatusesJob 
    {
        
        /// <summary>
        /// Actually runs the Job, we will get the AX Order Numbers that need updated from our database, get the statuses from AX, then update our db with those values from AX.
        /// </summary>
        /// <param name="context">The jobs execution context.</param>
        public void Execute()
        {
            AXJobLog jobLog;
            try
            {
                List<string> orderNumbers = GetAXOrderNumbersProcessingOrBackordered();
                //Log.Debug(String.Format("[UpdateStatusesJob] Getting statuses for {0} orders from AX", orderNumbers.Count));

                if (orderNumbers.Any())
                {

                    //Create the AX Client
                    AXSyncService.WS_AXProxy.AXProxyClient client = new AXSyncService.WS_AXProxy.AXProxyClient();

                    //Get the order statuses from AX
                    List<OrderStatus> statuses = new List<OrderStatus>();

                    bool updatedSuccessfully;
                    foreach (string orderNumber in orderNumbers)
                    {
                        try
                        {
                            OrderStatus status;
                            if (orderNumber.StartsWith("US"))
                            {
                                status = TrackDownOrder(orderNumber);
                                status.OrderNumber = status.AxOrderNumber; //this is to help umbraco map it correctly
                            }
                            else
                            {
                                status = client.GetOrderStatus(orderNumber);
                            }
                            statuses.Add(status);
                        }
                        catch (Exception ex)
                        {
                          //  Log.Debug(String.Format("[UpdateStatusesJob] Error getting order id {0}: {1}", orderNumber, ex.Message));

                        }
                        if (statuses.Count >= 100)
                        {
                           // Log.Debug(String.Format("[UpdateStatusesJob] Sending batch of {0} statuses to Umbraco", statuses.Count));
                            updatedSuccessfully = UpdateOrderStatuses(statuses.ToArray(), true);
                            statuses.Clear();
                        }
                    }

                    if (statuses.Count > 0)
                    {
                       // Log.Debug(String.Format("[UpdateStatusesJob] Sending batch of {0} statuses to Umbraco", statuses.Count));
                        updatedSuccessfully = UpdateOrderStatuses(statuses.ToArray(), true);
                    }

                  //  Log.Debug(String.Format("[UpdateStatusesJob] Done processing statuses"));
                }

                jobLog = new AXJobLog()
                {
                    Job = "AX Order Statuses",
                    Status = "Success",
                    Date = DateTime.Now
                };

            }
            catch (AggregateException ex)
            {

             //   Log.Debug(String.Format("[UpdateStatusesJob] Error getting order statuses! {0}", ex.Message));
                foreach(Exception inner in ex.InnerExceptions)
                {
                  //  Log.Debug(String.Format("[UpdateStatusesJob] Error getting order statuses! {0}", inner.Message));
                    Exception inner2 = inner.InnerException;
                    while (inner2 != null)
                    {
                      //  Log.Debug(String.Format("[UpdateStatusesJob] Error getting order statuses! {0}", inner2.Message));
                        inner2 = inner2.InnerException;
                    }
                }
                jobLog = new AXJobLog()
                {
                    Job = "AX Order Statuses",
                    Status = "Failed",
                    Date = DateTime.Now
                };
            }
            catch(Exception ex)
            {

             //   Log.Debug(String.Format("[UpdateStatusesJob] Error getting order statuses! {0}", ex.Message));
                Exception inner = ex.InnerException;
                while(inner!=null)
                {
                    //Log.Debug(String.Format("[UpdateStatusesJob] Error getting order statuses! {0}", inner.Message));
                    inner = inner.InnerException;
                }
                jobLog = new AXJobLog()
                {
                    Job = "AX Order Statuses",
                    Status = "Failed",
                    Date = DateTime.Now
                };
            }

            var db = new UmbracoEntities();
            db.AXJobLogs.Add(jobLog);
            db.Database.ExecuteSqlCommand("DELETE FROM AXJobLog WHERE Date <= DATEADD(dd, -30, GETDATE())");
            db.SaveChanges();
            //bool updatedTrackingSuccessfully = UpdateOrderTrackingNumbers(statuses);

        }

        private OrderStatus TrackDownOrder(string orderNumber)
        {
            UmbracoEntities db = new UmbracoEntities();
            AXSyncService.WS_AXProxy.AXProxyClient client = new AXSyncService.WS_AXProxy.AXProxyClient();
            OrderStatus orderStatus = new OrderStatus()
            {
                OrderNumber = orderNumber,
                AxOrderNumber = orderNumber,
                Status = "Not Found"
            };

            var order = db.TeaCommerce_Order.FirstOrDefault(o => o.OrderNumber == orderNumber);
            if(order!=null)
            {
                var advocates = db.Database.SqlQuery<String>("SELECT [Value] FROM [dbo].TeaCommerce_CustomOrderLineProperty WHERE alias='advocateItem' AND Value = 'true' AND OrderLineId IN (SELECT [Id] FROM [dbo].TeaCommerce_OrderLine WHERE OrderId=@p0)", new object[1] { order.Id });

                string orderType = "I";
                if (advocates.Any())
                    orderType = "A";

                var paymentMode = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.OrderId == order.Id && p.Alias == "cardType");

                if(paymentMode!=null)
                {
                    orderStatus = client.TrackDownOrder(orderType, orderNumber, paymentMode.Value);
                    if(orderStatus.AxOrderNumber!=orderNumber)
                    {
                        AddOrUpdateOrderProperty(db, orderNumber, "AXOrderNumber", orderStatus.AxOrderNumber);
                    }
                }

            }

            return orderStatus;
        }



        private void AddOrUpdateOrderProperty(UmbracoEntities db, string orderNumber, string alias, string value)
        {

            var prop = db.TeaCommerce_CustomOrderProperty.FirstOrDefault(p => p.TeaCommerce_Order.OrderNumber == orderNumber && p.Alias == alias);
            if (prop != null)
            {
                prop.Value = value;
                db.Entry(prop).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                prop = new TeaCommerce_CustomOrderProperty()
                {
                    OrderId = db.TeaCommerce_Order.First(o => o.OrderNumber == orderNumber).Id,
                    Alias = alias,
                    Value = value,
                    IsReadOnly = true,
                    ServerSideOnly = true
                };
                db.TeaCommerce_CustomOrderProperty.Add(prop);
                db.SaveChanges();
            }
        }

        private bool UpdateOrderTrackingNumbers(OrderStatus[] statuses)
        {
            //try
            //{
            //    var api = ApiRequestHelper.NewRequest("UpdateOrderStatuses");
            //    using (var client = new HttpClient())
            //    {

            //        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["HomeUrl"]);

            //        var apiEnd = String.Format("Umbraco/Api/AXEntryApi/UpdateOrderStatuses?request={0}", api.RequestID);

            //        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));


            //        var task = client.PostAsJsonAsync(apiEnd, statuses);
            //        task.Wait();

            //        var result = task.Result;

            //        string status = "";
            //        bool success = false;

            //        var readtask = result.Content.ReadAsAsync<string>();
            //        readtask.Wait();
            //        status = readtask.Result;
            //        Boolean.TryParse(status, out success);

            //        return success;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Log.Debug(String.Format("[UpdateStatusesJob] Error sending shipping information to {0}: {1}", ConfigurationManager.AppSettings["HomeUrl"], ex.Message));
            //    //TODO: Log exception
            //    throw ex;
            //}
            return true;
        }

        
        /// <summary>
        /// Makes a call to the AXEntryApi/UpdateOrderStatuses, which will update the order status of the TeaCommerce Order.
        /// </summary>
        /// <param name="statuses">The order statuses from AX</param>
        /// <param name="orderNumberIsAXOrderNumber">Need this in here because the get order statuses in the AX proxy returns the Ax Order Number in the Ordernumber field</param>
        /// <returns>True if there were no errors on the API side. False, if there were errors (not necessarily fatal)</returns>
        private bool UpdateOrderStatuses(OrderStatus[] statusesIn, bool orderNumberIsAXOrderNumberIn)
        {
            //try
            //{
            //    using (var client = new HttpClient())
            //    {

            //        var api = ApiRequestHelper.NewRequest("UpdateOrderStatuses");
            //        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["HomeUrl"]);

            //        var apiEnd = String.Format("Umbraco/Api/AXEntryApi/UpdateOrderStatuses?request={0}", api.RequestID);

            //        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //        var requestModel = new { OrderNumberIsAXOrderNumber = orderNumberIsAXOrderNumberIn, Statuses = statusesIn };

            //        var result = client.PostAsJsonAsync(apiEnd, requestModel).Result;

            //        bool status = result.Content.ReadAsAsync<bool>().Result;

            //        return status;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Log.Debug(String.Format("[UpdateStatusesJob] Error sending statuses to {0}: {1}", ConfigurationManager.AppSettings["HomeUrl"], ex.Message));
            //    //TODO: Log exception
            //    throw ex;
            //}

            return true;
        }


        /// <summary>
        /// Calls the AXEntryApi/GetAXOrderNumbersProcessingOrBackordered which will return a list of AX order numbers whose status is Processing or Backordered.
        /// </summary>
        /// <returns>List of AX Order Numbers</returns>
        private List<string> GetAXOrderNumbersProcessingOrBackordered()
        {
            //try
            //{
            //    var api = ApiRequestHelper.NewRequest("GetAXOrderNumbersProcessingOrBackordered");
            //    using (var client = new HttpClient())
            //    {

            //        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["HomeUrl"]);

            //        var apiEnd = String.Format("Umbraco/Api/AXEntryApi/GetAXOrderNumbersProcessingOrBackordered?request={0}", api.RequestID);

            //        //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //        var result = client.GetAsync(apiEnd).Result;

            //        List<string> orderNumbers = result.Content.ReadAsAsync<List<string>>().Result;

            //        return orderNumbers ?? new List<string>();
            //    }
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
            return null;
        }
    }
}