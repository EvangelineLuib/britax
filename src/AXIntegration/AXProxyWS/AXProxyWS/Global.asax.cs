﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace AXProxyWS
{
    using System.IO;
    using System.Web.Configuration;

    using Autofac;

    using AXIOC;

    using Serilog;

    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.InitializeBuilder();
            var diagnosticUrl = WebConfigurationManager.AppSettings["diagnostic"];
            var diagnostic_logToFile = false;
            Boolean.TryParse(WebConfigurationManager.AppSettings["diagnostic_logtofile"], out diagnostic_logToFile);

            string logOutputTemplate = "{Timestamp} [{Level}] ({Name}){NewLine} {Message}{NewLine}{Exception}";
            string logPathFormat = Path.Combine(HttpRuntime.AppDomainAppPath, @"Logs\Credit.Log-{Date}.txt");
            var loggerConfig = new LoggerConfiguration();
            if (!String.IsNullOrEmpty(diagnosticUrl))
            {
                loggerConfig.WriteTo.Slack(diagnosticUrl);
            }
            else
            {
                diagnostic_logToFile = true;
            }

            if (diagnostic_logToFile)
            {
                loggerConfig.WriteTo.RollingFile(pathFormat: logPathFormat, outputTemplate: logOutputTemplate);
            }

            var logger = loggerConfig.CreateLogger();
            Bootstrapper.Builder.Register<ILogger>(ctx => logger).SingleInstance();

            Bootstrapper.SetAutofacContainer();

            var defaultLogger2 = Bootstrapper.Container.Resolve<ILogger>();

            defaultLogger2.Information("AX Started");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
