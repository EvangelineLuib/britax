﻿using AXWebServiceHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AXProxyWS
{
    using System.Configuration;

    using Autofac;

    using AXIOC;

    using Newtonsoft.Json;

    using Serilog;
    using Britax.Common.Models;

    public class AXProxy : IAXProxy
    {
        

        double IAXProxy.GetTaxRate(string zipCode)
        {
            return 0.0;
        }

        OrderStatus IAXProxy.GetOrderByCustomer(string customerName)
        {
            var returnValue= AXSalesOrderHelper.Instance.GetOrderByCustomer(customerName);
            this.SendLogs("GetOrderStatus", returnValue);
            return returnValue;
        }

        OrderStatus IAXProxy.GetOrderStatus(string orderNumber)
        {
            var returnValue= AXSalesOrderHelper.Instance.GetOrderStatus(orderNumber);
            this.SendLogs("GetOrderStatus", returnValue);

            return returnValue;
        }

        OrderStatus[] IAXProxy.GetOrderStatuses(string[] orderNumbers)
        {
            var returnValue= AXSalesOrderHelper.Instance.GetOrderStatuses(orderNumbers);
            this.SendLogs("RETURN AXPROXY GetOrderStatuses", returnValue);
            return returnValue;
        }

        InventoryLevel[] IAXProxy.GetInventoryLevels(string[] skus)
        {
            var returnValue= AXInventoryHelper.Instance.GetInventoryLevels(skus);
            //this.SendLogs("GETINVENTORYLEVEL", returnValue);
            return returnValue;
        }


        InventoryLevel IAXProxy.GetInventoryLevel(string sku)
        {
           
            var returnValue= AXInventoryHelper.Instance.GetInventoryLevel(sku);
           // this.SendLogs("GETINVENTORYLEVEL", returnValue);

            return returnValue;
        }

        OrderStatus[] IAXProxy.SubmitOrders(OrderSubmission[] orders)
        {
            this.SendLogs("AX PROXY SUBMIT ORDER", orders);
             var orderStatus= AXSalesOrderHelper.Instance.SubmitOrders(orders);
            this.SendLogs("AX PROXY SUBMIT ORDER RETURN STATUS", orderStatus);
            return orderStatus;
        }


        OrderStatus IAXProxy.TrackDownOrder(string orderType, string orderNumber, string paymentMode)
        {
            var returnValue = AXSalesOrderHelper.Instance.TrackDownOrder(orderType, orderNumber, paymentMode);

            this.SendLogs("TrackDownOrder", returnValue);

            return returnValue;
        }

        public InventoryLevel[] GetInventoryLevelsFromTA(List<InventoryLevel> skus, string axAccountRelation, string axDataAreaId)
        {

           var returnValue =AXInventoryHelper.Instance.GetInventoryLevelsFromTA(skus, axAccountRelation, axDataAreaId);
            
          // this.SendLogs("GetInventoryLevelsFromTradeAgreement", returnValue);

            return returnValue;
        }
		
		SkuWithName[] IAXProxy.GetInventorySkus(int batchNumber, int batchSize)
		{
            var returnValue=AXInventoryHelper.Instance.GetInventorySkus(batchNumber, batchSize);
		  //  this.SendLogs("GETINVENTORYSKU", returnValue);
		    return returnValue;
		}

        void SendLogs(string methodname, object logObject)
        {
            var valueSerialize = JsonConvert.SerializeObject(logObject);
            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
            defaultLogger.Information($"{methodname}  ==> {valueSerialize}");
        }

        public OrderStatus[] GetOrderStatusByList(OrderStatus[] orderStatusList)
        {
            var returnValue = AXSalesOrderHelper.Instance.GetOrderStatuses(orderStatusList.ToList());
            this.SendLogs("RETURN TO  AXPROXY  List<OrderStatus> GetOrderStatusByList ", returnValue);
            return returnValue.ToArray();
        }
    }
}
