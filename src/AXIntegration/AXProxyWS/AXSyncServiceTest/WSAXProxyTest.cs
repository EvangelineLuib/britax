﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AXSyncServiceTest
{
    using System.Collections.Generic;
    using System.Linq;

    using AXSyncServiceTest.WS_AXProxy;

    [TestClass]
    public class WSAXProxyTest
    {
        [TestMethod]
        public void Test_WS()
        {
            var proxyClient = new  WS_AXProxy.AXProxyClient();
            var newOrder = new OrderStatus
            {
                OrderId = Guid.NewGuid(),
                AxOrderNumber = "S190396",
                OrderNumber = "NZ1"

            };

            List<OrderStatus> orderStatuslist = new List<OrderStatus>();

            orderStatuslist.Add(newOrder);


            var returnStatus = proxyClient.GetOrderStatusByList(orderStatuslist.ToArray());
            var status = returnStatus.FirstOrDefault();

        }
    }
}
