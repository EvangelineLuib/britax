﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AXWebServiceHelper.WS_PriceListService {
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Fault")]
    public partial class AifFault : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string customDetailXmlField;
        
        private FaultMessageList[] faultMessageListArrayField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string CustomDetailXml {
            get {
                return this.customDetailXmlField;
            }
            set {
                this.customDetailXmlField = value;
                this.RaisePropertyChanged("CustomDetailXml");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true, Order=1)]
        public FaultMessageList[] FaultMessageListArray {
            get {
                return this.faultMessageListArrayField;
            }
            set {
                this.faultMessageListArrayField = value;
                this.RaisePropertyChanged("FaultMessageListArray");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Fault")]
    public partial class FaultMessageList : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string documentField;
        
        private string documentOperationField;
        
        private FaultMessage[] faultMessageArrayField;
        
        private string fieldField;
        
        private string serviceField;
        
        private string serviceOperationField;
        
        private string serviceOperationParameterField;
        
        private string xPathField;
        
        private string xmlLineField;
        
        private string xmlPositionField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string Document {
            get {
                return this.documentField;
            }
            set {
                this.documentField = value;
                this.RaisePropertyChanged("Document");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string DocumentOperation {
            get {
                return this.documentOperationField;
            }
            set {
                this.documentOperationField = value;
                this.RaisePropertyChanged("DocumentOperation");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable=true, Order=2)]
        public FaultMessage[] FaultMessageArray {
            get {
                return this.faultMessageArrayField;
            }
            set {
                this.faultMessageArrayField = value;
                this.RaisePropertyChanged("FaultMessageArray");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=3)]
        public string Field {
            get {
                return this.fieldField;
            }
            set {
                this.fieldField = value;
                this.RaisePropertyChanged("Field");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=4)]
        public string Service {
            get {
                return this.serviceField;
            }
            set {
                this.serviceField = value;
                this.RaisePropertyChanged("Service");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=5)]
        public string ServiceOperation {
            get {
                return this.serviceOperationField;
            }
            set {
                this.serviceOperationField = value;
                this.RaisePropertyChanged("ServiceOperation");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=6)]
        public string ServiceOperationParameter {
            get {
                return this.serviceOperationParameterField;
            }
            set {
                this.serviceOperationParameterField = value;
                this.RaisePropertyChanged("ServiceOperationParameter");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=7)]
        public string XPath {
            get {
                return this.xPathField;
            }
            set {
                this.xPathField = value;
                this.RaisePropertyChanged("XPath");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=8)]
        public string XmlLine {
            get {
                return this.xmlLineField;
            }
            set {
                this.xmlLineField = value;
                this.RaisePropertyChanged("XmlLine");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=9)]
        public string XmlPosition {
            get {
                return this.xmlPositionField;
            }
            set {
                this.xmlPositionField = value;
                this.RaisePropertyChanged("XmlPosition");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Fault")]
    public partial class FaultMessage : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string codeField;
        
        private string messageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
                this.RaisePropertyChanged("Code");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
                this.RaisePropertyChanged("Message");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
    public partial class AxdEntity_SalesPrice : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string _DocumentHashField;
        
        private decimal amountField;
        
        private string configIdField;
        
        private string currencyField;
        
        private string custAccountField;
        
        private string custNameField;
        
        private string inventColorIdField;
        
        private string inventSizeIdField;
        
        private string itemGroupIdField;
        
        private string itemIdField;
        
        private string itemNameField;
        
        private System.Nullable<AxdEnum_ItemType> itemTypeField;
        
        private bool itemTypeFieldSpecified;
        
        private System.DateTime priceDateField;
        
        private System.Nullable<decimal> priceUnitField;
        
        private bool priceUnitFieldSpecified;
        
        private System.Nullable<decimal> quantityField;
        
        private bool quantityFieldSpecified;
        
        private System.Nullable<long> recIdField;
        
        private bool recIdFieldSpecified;
        
        private System.Nullable<int> recVersionField;
        
        private bool recVersionFieldSpecified;
        
        private string unitIdField;
        
        private string classField;
        
        private AxdEnum_AxdEntityAction actionField;
        
        private bool actionFieldSpecified;
        
        public AxdEntity_SalesPrice() {
            this.classField = "entity";
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string _DocumentHash {
            get {
                return this._DocumentHashField;
            }
            set {
                this._DocumentHashField = value;
                this.RaisePropertyChanged("_DocumentHash");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public decimal Amount {
            get {
                return this.amountField;
            }
            set {
                this.amountField = value;
                this.RaisePropertyChanged("Amount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public string ConfigId {
            get {
                return this.configIdField;
            }
            set {
                this.configIdField = value;
                this.RaisePropertyChanged("ConfigId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Currency {
            get {
                return this.currencyField;
            }
            set {
                this.currencyField = value;
                this.RaisePropertyChanged("Currency");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string CustAccount {
            get {
                return this.custAccountField;
            }
            set {
                this.custAccountField = value;
                this.RaisePropertyChanged("CustAccount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=5)]
        public string CustName {
            get {
                return this.custNameField;
            }
            set {
                this.custNameField = value;
                this.RaisePropertyChanged("CustName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=6)]
        public string InventColorId {
            get {
                return this.inventColorIdField;
            }
            set {
                this.inventColorIdField = value;
                this.RaisePropertyChanged("InventColorId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=7)]
        public string InventSizeId {
            get {
                return this.inventSizeIdField;
            }
            set {
                this.inventSizeIdField = value;
                this.RaisePropertyChanged("InventSizeId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=8)]
        public string ItemGroupId {
            get {
                return this.itemGroupIdField;
            }
            set {
                this.itemGroupIdField = value;
                this.RaisePropertyChanged("ItemGroupId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=9)]
        public string ItemId {
            get {
                return this.itemIdField;
            }
            set {
                this.itemIdField = value;
                this.RaisePropertyChanged("ItemId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=10)]
        public string ItemName {
            get {
                return this.itemNameField;
            }
            set {
                this.itemNameField = value;
                this.RaisePropertyChanged("ItemName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=11)]
        public System.Nullable<AxdEnum_ItemType> ItemType {
            get {
                return this.itemTypeField;
            }
            set {
                this.itemTypeField = value;
                this.RaisePropertyChanged("ItemType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ItemTypeSpecified {
            get {
                return this.itemTypeFieldSpecified;
            }
            set {
                this.itemTypeFieldSpecified = value;
                this.RaisePropertyChanged("ItemTypeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date", Order=12)]
        public System.DateTime PriceDate {
            get {
                return this.priceDateField;
            }
            set {
                this.priceDateField = value;
                this.RaisePropertyChanged("PriceDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=13)]
        public System.Nullable<decimal> PriceUnit {
            get {
                return this.priceUnitField;
            }
            set {
                this.priceUnitField = value;
                this.RaisePropertyChanged("PriceUnit");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PriceUnitSpecified {
            get {
                return this.priceUnitFieldSpecified;
            }
            set {
                this.priceUnitFieldSpecified = value;
                this.RaisePropertyChanged("PriceUnitSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=14)]
        public System.Nullable<decimal> Quantity {
            get {
                return this.quantityField;
            }
            set {
                this.quantityField = value;
                this.RaisePropertyChanged("Quantity");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantitySpecified {
            get {
                return this.quantityFieldSpecified;
            }
            set {
                this.quantityFieldSpecified = value;
                this.RaisePropertyChanged("QuantitySpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=15)]
        public System.Nullable<long> RecId {
            get {
                return this.recIdField;
            }
            set {
                this.recIdField = value;
                this.RaisePropertyChanged("RecId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RecIdSpecified {
            get {
                return this.recIdFieldSpecified;
            }
            set {
                this.recIdFieldSpecified = value;
                this.RaisePropertyChanged("RecIdSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=16)]
        public System.Nullable<int> RecVersion {
            get {
                return this.recVersionField;
            }
            set {
                this.recVersionField = value;
                this.RaisePropertyChanged("RecVersion");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RecVersionSpecified {
            get {
                return this.recVersionFieldSpecified;
            }
            set {
                this.recVersionFieldSpecified = value;
                this.RaisePropertyChanged("RecVersionSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=17)]
        public string UnitId {
            get {
                return this.unitIdField;
            }
            set {
                this.unitIdField = value;
                this.RaisePropertyChanged("UnitId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string @class {
            get {
                return this.classField;
            }
            set {
                this.classField = value;
                this.RaisePropertyChanged("class");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public AxdEnum_AxdEntityAction action {
            get {
                return this.actionField;
            }
            set {
                this.actionField = value;
                this.RaisePropertyChanged("action");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool actionSpecified {
            get {
                return this.actionFieldSpecified;
            }
            set {
                this.actionFieldSpecified = value;
                this.RaisePropertyChanged("actionSpecified");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
    public enum AxdEnum_ItemType {
        
        /// <remarks/>
        Item,
        
        /// <remarks/>
        BOM,
        
        /// <remarks/>
        Service,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
    public enum AxdEnum_AxdEntityAction {
        
        /// <remarks/>
        create,
        
        /// <remarks/>
        update,
        
        /// <remarks/>
        replace,
        
        /// <remarks/>
        delete,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
    public partial class AxdPricelist : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string currencyField;
        
        private string custAccountField;
        
        private System.Nullable<AxdEnum_XMLDocPurpose> docPurposeField;
        
        private bool docPurposeFieldSpecified;
        
        private System.Nullable<System.DateTime> priceDateField;
        
        private bool priceDateFieldSpecified;
        
        private string senderIdField;
        
        private AxdEntity_SalesPrice[] salesPriceField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=0)]
        public string Currency {
            get {
                return this.currencyField;
            }
            set {
                this.currencyField = value;
                this.RaisePropertyChanged("Currency");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=1)]
        public string CustAccount {
            get {
                return this.custAccountField;
            }
            set {
                this.custAccountField = value;
                this.RaisePropertyChanged("CustAccount");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=2)]
        public System.Nullable<AxdEnum_XMLDocPurpose> DocPurpose {
            get {
                return this.docPurposeField;
            }
            set {
                this.docPurposeField = value;
                this.RaisePropertyChanged("DocPurpose");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DocPurposeSpecified {
            get {
                return this.docPurposeFieldSpecified;
            }
            set {
                this.docPurposeFieldSpecified = value;
                this.RaisePropertyChanged("DocPurposeSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date", IsNullable=true, Order=3)]
        public System.Nullable<System.DateTime> PriceDate {
            get {
                return this.priceDateField;
            }
            set {
                this.priceDateField = value;
                this.RaisePropertyChanged("PriceDate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PriceDateSpecified {
            get {
                return this.priceDateFieldSpecified;
            }
            set {
                this.priceDateFieldSpecified = value;
                this.RaisePropertyChanged("PriceDateSpecified");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true, Order=4)]
        public string SenderId {
            get {
                return this.senderIdField;
            }
            set {
                this.senderIdField = value;
                this.RaisePropertyChanged("SenderId");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SalesPrice", Order=5)]
        public AxdEntity_SalesPrice[] SalesPrice {
            get {
                return this.salesPriceField;
            }
            set {
                this.salesPriceField = value;
                this.RaisePropertyChanged("SalesPrice");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
    public enum AxdEnum_XMLDocPurpose {
        
        /// <remarks/>
        Original,
        
        /// <remarks/>
        Duplicate,
        
        /// <remarks/>
        Proforma,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria")]
    public partial class CriteriaElement : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string dataSourceNameField;
        
        private string fieldNameField;
        
        private Operator operatorField;
        
        private string value1Field;
        
        private string value2Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string DataSourceName {
            get {
                return this.dataSourceNameField;
            }
            set {
                this.dataSourceNameField = value;
                this.RaisePropertyChanged("DataSourceName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string FieldName {
            get {
                return this.fieldNameField;
            }
            set {
                this.fieldNameField = value;
                this.RaisePropertyChanged("FieldName");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public Operator Operator {
            get {
                return this.operatorField;
            }
            set {
                this.operatorField = value;
                this.RaisePropertyChanged("Operator");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string Value1 {
            get {
                return this.value1Field;
            }
            set {
                this.value1Field = value;
                this.RaisePropertyChanged("Value1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=4)]
        public string Value2 {
            get {
                return this.value2Field;
            }
            set {
                this.value2Field = value;
                this.RaisePropertyChanged("Value2");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria")]
    public enum Operator {
        
        /// <remarks/>
        Equal,
        
        /// <remarks/>
        NotEqual,
        
        /// <remarks/>
        Greater,
        
        /// <remarks/>
        GreaterOrEqual,
        
        /// <remarks/>
        Less,
        
        /// <remarks/>
        LessOrEqual,
        
        /// <remarks/>
        Range,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria")]
    public partial class QueryCriteria : object, System.ComponentModel.INotifyPropertyChanged {
        
        private CriteriaElement[] criteriaElementField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CriteriaElement", Order=0)]
        public CriteriaElement[] CriteriaElement {
            get {
                return this.criteriaElementField;
            }
            set {
                this.criteriaElementField = value;
                this.RaisePropertyChanged("CriteriaElement");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/services", ConfigurationName="WS_PriceListService.PriceListService")]
    public interface PriceListService {
        
        // CODEGEN: Generating message contract since the wrapper name (PriceListServiceFindRequest) of message PriceListServiceFindRequest does not match the default value (find)
        [System.ServiceModel.OperationContractAttribute(Action="http://schemas.microsoft.com/dynamics/2008/01/services/PriceListService/find", ReplyAction="http://schemas.microsoft.com/dynamics/2008/01/services/PriceListService/findRespo" +
            "nse")]
        [System.ServiceModel.FaultContractAttribute(typeof(AXWebServiceHelper.WS_PriceListService.AifFault), Action="http://schemas.microsoft.com/dynamics/2008/01/services/PriceListService/findAifFa" +
            "ultFault", Name="AifFault", Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Fault")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse find(AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://schemas.microsoft.com/dynamics/2008/01/services/PriceListService/find", ReplyAction="http://schemas.microsoft.com/dynamics/2008/01/services/PriceListService/findRespo" +
            "nse")]
        System.Threading.Tasks.Task<AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse> findAsync(AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="PriceListServiceFindRequest", WrapperNamespace="http://schemas.microsoft.com/dynamics/2008/01/services", IsWrapped=true)]
    public partial class PriceListServiceFindRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.microsoft.com/dynamics/2006/02/documents/QueryCriteria")]
        public AXWebServiceHelper.WS_PriceListService.QueryCriteria QueryCriteria;
        
        public PriceListServiceFindRequest() {
        }
        
        public PriceListServiceFindRequest(AXWebServiceHelper.WS_PriceListService.QueryCriteria QueryCriteria) {
            this.QueryCriteria = QueryCriteria;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="PriceListServiceFindResponse", WrapperNamespace="http://schemas.microsoft.com/dynamics/2008/01/services", IsWrapped=true)]
    public partial class PriceListServiceFindResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist", Order=0)]
        [System.Xml.Serialization.XmlElementAttribute(Namespace="http://schemas.microsoft.com/dynamics/2008/01/documents/Pricelist")]
        public AXWebServiceHelper.WS_PriceListService.AxdPricelist Pricelist;
        
        public PriceListServiceFindResponse() {
        }
        
        public PriceListServiceFindResponse(AXWebServiceHelper.WS_PriceListService.AxdPricelist Pricelist) {
            this.Pricelist = Pricelist;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface PriceListServiceChannel : AXWebServiceHelper.WS_PriceListService.PriceListService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PriceListServiceClient : System.ServiceModel.ClientBase<AXWebServiceHelper.WS_PriceListService.PriceListService>, AXWebServiceHelper.WS_PriceListService.PriceListService {
        
        public PriceListServiceClient() {
        }
        
        public PriceListServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PriceListServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PriceListServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PriceListServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse AXWebServiceHelper.WS_PriceListService.PriceListService.find(AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest request) {
            return base.Channel.find(request);
        }
        
        public AXWebServiceHelper.WS_PriceListService.AxdPricelist find(AXWebServiceHelper.WS_PriceListService.QueryCriteria QueryCriteria) {
            AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest inValue = new AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest();
            inValue.QueryCriteria = QueryCriteria;
            AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse retVal = ((AXWebServiceHelper.WS_PriceListService.PriceListService)(this)).find(inValue);
            return retVal.Pricelist;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse> AXWebServiceHelper.WS_PriceListService.PriceListService.findAsync(AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest request) {
            return base.Channel.findAsync(request);
        }
        
        public System.Threading.Tasks.Task<AXWebServiceHelper.WS_PriceListService.PriceListServiceFindResponse> findAsync(AXWebServiceHelper.WS_PriceListService.QueryCriteria QueryCriteria) {
            AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest inValue = new AXWebServiceHelper.WS_PriceListService.PriceListServiceFindRequest();
            inValue.QueryCriteria = QueryCriteria;
            return ((AXWebServiceHelper.WS_PriceListService.PriceListService)(this)).findAsync(inValue);
        }
    }
}
