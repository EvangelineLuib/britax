﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using AXWebServiceHelper.WS_SalesOrderService;

namespace AXWebServiceHelper
{
    using Autofac;

    using AXIOC;

    using Newtonsoft.Json;

    using Serilog;
    using System.Web.Configuration;

    using Britax.Common.Models;

    public sealed class AXSalesOrderHelper
    {
        private static readonly Lazy<AXSalesOrderHelper> lazy =
         new Lazy<AXSalesOrderHelper>(() => new AXSalesOrderHelper());

        public static AXSalesOrderHelper Instance { get { return lazy.Value; } }

        private WS_SalesOrderService.SalesOrderServiceClient clientAXSalesOrder
        {
            get
            {
                WS_SalesOrderService.SalesOrderServiceClient clientAXSalesOrder = new WS_SalesOrderService.SalesOrderServiceClient();


                var ax_username = WebConfigurationManager.AppSettings["ax_username"].ToString();
                var ax_password = WebConfigurationManager.AppSettings["ax_password"].ToString();
                clientAXSalesOrder.ClientCredentials.Windows.ClientCredential.UserName = ax_username;
                clientAXSalesOrder.ClientCredentials.Windows.ClientCredential.Password = ax_password;


                clientAXSalesOrder.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

                return clientAXSalesOrder;
            }
        }

        public List<OrderStatus> GetOrderStatuses(List<OrderStatus>  orderStatusList)
        {
            var client = this.SalesClient;
            var axEndpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            OperationContextScope o = new OperationContextScope(client.InnerChannel);
            var mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", axEndpoint);
            OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
            OperationContext.Current.OutgoingMessageHeaders.Add(mh);

            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();

            // define QueryCriteria that will be used for finding data in AX
            WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
            WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "SalesId";
            qe[0].Operator = WS_SalesOrderService.Operator.Equal;



            var statusesReturn = new List<OrderStatus>();

            foreach (var  orderStatusLine in orderStatusList)
            {
                string status = "Not Found";


                qe[0].Value1 = orderStatusLine.AxOrderNumber;
                qc.CriteriaElement = qe;

                try
                {
                    // findKeys service operation will return list of unique fields that meet query criteria
                    defaultLogger.Information($"FIND {orderStatusLine.AxOrderNumber}");
                    var entityKey = client.findKeys(qc);

                    if (entityKey!=null)
                    {
                        defaultLogger.Information($"FOUND  KEY {orderStatusLine.AxOrderNumber}");
                        WS_SalesOrderService.AxdSalesOrder order = client.read(entityKey);

                        if (order != null)
                        {
                            defaultLogger.Information($"FOUND  ORDER {orderStatusLine.AxOrderNumber}");

                            var salesTableData = order.SalesTable.FirstOrDefault();

                            if (salesTableData != null)
                            {
                                status = salesTableData.SalesStatus?.ToString() ?? AxdEnum_SalesStatus.None.ToString();
                            }
                            else
                            {
                                status = AxdEnum_SalesStatus.None.ToString();
                            }

                            defaultLogger.Information($"FOUND  ORDER {orderStatusLine.AxOrderNumber}  {status}");

                        }
                        else
                        {
                            defaultLogger.Information($"NOT FOUND  ORDER {orderStatusLine.AxOrderNumber}");
                            status = WS_SalesOrderService.AxdEnum_SalesStatus.Canceled.ToString();

                        }

                    }
                    else
                    {
                        defaultLogger.Information($"FOUND  {orderStatusLine.AxOrderNumber}");
                        //Couldn't find the order number in ax, so it must be deleted, cancel the order on our end
                        status = WS_SalesOrderService.AxdEnum_SalesStatus.Canceled.ToString();
                    }

                    statusesReturn.Add(new OrderStatus()
                    {
                        OrderId = orderStatusLine.OrderId,
                        AxOrderNumber = orderStatusLine.AxOrderNumber,
                        OrderNumber = orderStatusLine.OrderNumber,
                        Status = status,
                        ShippingNumber = orderStatusLine.ShippingNumber
                    });
                }
                catch (Exception exx)
                {
                    var defaultLogger2 = Bootstrapper.Container.Resolve<ILogger>();
                    defaultLogger2.Error(exx, "ERROR PROXY -in List<OrderStatus> GetOrderStatuses(List<OrderStatus>  orderStatusList) ");
                }
            }


            return statusesReturn;

        }
        public OrderStatus[] GetOrderStatuses(string[] orderNumbers)
        {
            var client = this.SalesClient;
            var ax_endpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            OperationContextScope o = new OperationContextScope(client.InnerChannel);
            var mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", ax_endpoint);
            OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
            OperationContext.Current.OutgoingMessageHeaders.Add(mh);

            List<OrderStatus> statuses = new List<OrderStatus>();

            foreach (string orderId in orderNumbers)
            {
                string status = "Not Found";
                List<String> ship = new List<String>();
                // define QueryCriteria that will be used for finding data in AX
                WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
                WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

                // define entity key that will contain value of unique identifier of a record
                WS_SalesOrderService.EntityKey[] entityKey = { new WS_SalesOrderService.EntityKey() };

                // example of criteria element for a query
                qe[0].DataSourceName = "SalesTable";
                qe[0].FieldName = "SalesId";
                qe[0].Operator = WS_SalesOrderService.Operator.Equal;
                qe[0].Value1 = orderId;
                qc.CriteriaElement = qe;

                try
                {
                    // findKeys service operation will return list of unique fields that meet query criteria
                    entityKey = client.findKeys(qc);
                    if (null != entityKey)
                    {

                        WS_SalesOrderService.AxdSalesOrder order = client.read(entityKey);

                        if (order != null && order.SalesTable.Length > 0)
                        {
                            var table = order.SalesTable[0];
                            if (table.SalesStatus.HasValue)
                            {
                                status = table.SalesStatus.Value.ToString();
                            }
                            ship.Add(table.BEHDeliveryNum);
                            foreach (var line in table.SalesLine)
                            {
                                ship.Add(line.BEHDeliveryNum);
                            }
                        }
                    }
                    else
                    {
                        //Couldn't find the order number in ax, so it must be deleted, cancel the order on our end
                        status = WS_SalesOrderService.AxdEnum_SalesStatus.Canceled.ToString();
                    }
                    statuses.Add(new OrderStatus()
                    {
                        AxOrderNumber = orderId,
                        OrderNumber = orderId,
                        Status = status,
                        ShippingNumber = ship.Distinct()
                    });
                }
                catch (Exception  exx)
                {
                    var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                    defaultLogger.Error(exx, " public orderStatus[] GetOrderStatuses(string[] orderNumbers)");
                }
            }
            return statuses.ToArray();
        }

        public OrderStatus TrackDownOrder(string orderType, string orderNumber, string paymentMode)
        {
            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
            WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record
            WS_SalesOrderService.EntityKey[] entityKey = { new WS_SalesOrderService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "CustomerRef";
            qe[0].Operator = WS_SalesOrderService.Operator.Equal;
            qe[0].Value1 = String.Format("{0}-{1} PT: {2}", orderType, orderNumber, paymentMode); //I-ORDER-16, e.g., followed by the credit card type;

            qc.CriteriaElement = qe;


            var client = this.SalesClient;
            var ax_endpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            OperationContextScope o = new OperationContextScope(client.InnerChannel);
            var mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", ax_endpoint);
            OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
            OperationContext.Current.OutgoingMessageHeaders.Add(mh);

            string axOrderNumber = "";
            try
            {

                WS_SalesOrderService.AxdSalesOrder order = client.find(qc);

                if (order != null && order.SalesTable != null && order.SalesTable.Length > 0)
                {
                    var table = order.SalesTable[0];
                    if (table.SalesStatus.HasValue)
                    {
                        axOrderNumber = table.SalesId;
                        status = table.SalesStatus.Value.ToString();
                    }
                    ship.Add(table.BEHDeliveryNum);
                    foreach (var line in table.SalesLine)
                    {
                        //if one line is delivered, mark the whole order delivered
                        if (line.SalesStatus == WS_SalesOrderService.AxdEnum_SalesStatus.Delivered)
                            status = line.SalesStatus.ToString();
                        ship.Add(line.BEHDeliveryNum);
                    }

                }

            }
            catch (Exception)
            {
            }
            return new OrderStatus()
            {
                AxOrderNumber = axOrderNumber,
                OrderNumber = orderNumber,
                Status = status,
                ShippingNumber = ship.Distinct()
            };
        }

        void SendLogs(string methodname, object logObject)
        {
            var valueSerialize = JsonConvert.SerializeObject(logObject);
            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
            defaultLogger.Information($"{methodname}  ==> {valueSerialize}");
        }

        public OrderStatus[] SubmitOrders(OrderSubmission[] orders)
        {


            var allSetting = WebConfigurationManager.AppSettings;
            this.SendLogs("PROXY -  orderStatus[] SubmitOrders(OrderSubmission[] orders)   SETTINGS == >", allSetting);

            var ax_web_customeraccount = WebConfigurationManager.AppSettings["ax_web_customeraccount"].ToString();
            var ax_endpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            var ax_deliverycountryregionId = WebConfigurationManager.AppSettings["ax_deliverycountryregionId"].ToString();
            var ax_taxgroup = WebConfigurationManager.AppSettings["ax_taxgroup"].ToString();
            var ax_SalesOriginId= WebConfigurationManager.AppSettings["ax_SalesOriginId"].ToString();
            var ax_TaxItemGroup = WebConfigurationManager.AppSettings["ax_TaxItemGroup"].ToString();
            var ax_MarkupCode = WebConfigurationManager.AppSettings["ax_MarkupCode"].ToString();
            var ax_BGTReasonCode = WebConfigurationManager.AppSettings["ax_BGTReasonCode"].ToString();
            var ax_CurrencyCode = WebConfigurationManager.AppSettings["ax_CurrencyCode"].ToString();
            var ax_IncludeTaxHeader = WebConfigurationManager.AppSettings["ax_includeTaxHeader"].ToString();

            bool isAx_includeTaxHeader = false;

            Boolean.TryParse(ax_IncludeTaxHeader, out isAx_includeTaxHeader);

            List < OrderStatus> statuses = new List<OrderStatus>();

            int dateadder = 1;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) dateadder += 1;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday) dateadder += 2;
            DateTime shipDate = DateTime.Now.AddDays(dateadder);

            var client = this.SalesClient;
            OperationContextScope o = new OperationContextScope(client.InnerChannel);
            var mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", ax_endpoint);

            OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
            OperationContext.Current.OutgoingMessageHeaders.Add(mh);



            foreach (var order in orders)
            {
                var customerReference=($"{order.OrderType}:{order.OrderNumber}:{order.PaymentMode}:{order.AuthorizationId}").Trim();
                var checkExistingOrder= this.GetOrderStatusByCustomerRef(customerReference);
                if (checkExistingOrder.OrderNumber != string.Empty)
                {
                    OrderAlreadyExistsAction(statuses, order, checkExistingOrder.AxOrderNumber);

                    continue;
                }
                else
                {
                    using (var dbAx = new AxExchange())
                    {
                        var axQuery = $"SELECT SALESSID FROM SALESTABLE WHERE CUSTOMERREF = '{customerReference}'";

                        var existingSalesId = dbAx.Database.SqlQuery<string>(axQuery).FirstOrDefault();
                        if (!string.IsNullOrEmpty(existingSalesId))
                        {
                            OrderAlreadyExistsAction(statuses, order, existingSalesId);

                            continue;
                        }

                        this.SendLogs("ORDER DOES NOT EXIST", order);
                    }
                }


                // Create an instance of the document class.
                AxdSalesOrder salesOrder = new WS_SalesOrderService.AxdSalesOrder();

                AxdEntity_SalesTable salesTable = new WS_SalesOrderService.AxdEntity_SalesTable();
                string endpoint = ax_endpoint;
                var taxGroup = ax_taxgroup;
                var splitString = order.OrderNumber.Split('-');


                salesTable.CurrencyCode = order.CurrencyCode;
                salesTable.DeliveryName = order.ShipName; //customer first name and last
                salesTable.ShipCarrierDeliveryContact = order.ShipPhone;
                salesTable.DeliveryCountryRegionId = ax_deliverycountryregionId;
                salesTable.DeliveryStreet = order.ShipStreet; //shipping street for order
                salesTable.DeliveryState = order?.ShipState.ToUpper(); //shipping state for order
                salesTable.DeliveryCity = order.ShipCity; //shipping state for order
                salesTable.DeliveryCounty = order.ShipCity?.ToUpper()?.Trim();

                if (salesTable.DeliveryCounty.Length > 10)
                {
                    salesTable.DeliveryCounty = salesTable.DeliveryCounty.Substring(0, 10);
                }

                salesTable.DeliveryZipCode = order.ShipZip; //shipping zip code for order

                salesTable.CreditCardProcessorTransactionId = order.AuthorizationId; //authorize.net transaction


                salesTable.CustAccount = ax_web_customeraccount; //two character state abbrev + NET03, typically (ex: VANET03)
                salesTable.InvoiceAccount = ax_web_customeraccount; //same as above


                salesTable.CustomerRef = customerReference;
                salesTable.PurchOrderFormNum = order.AuthorizationId; //I-ORDER-16, e.g., followed by the credit card type

                salesTable.SalesName = order.ShipName; //customer first name and last
                salesTable.Email = order.Email; //confirmation email address

                salesTable.ReceiptDateRequested = shipDate;
                salesTable.ReceiptDateRequestedSpecified = true;
                salesTable.ReceiptDateConfirmed = shipDate;
                salesTable.ReceiptDateConfirmedSpecified = true;
                salesTable.ShippingDateRequested = shipDate;
                salesTable.ShippingDateRequestedSpecified = true;
                salesTable.ShippingDateConfirmed = shipDate;
                salesTable.ShippingDateConfirmedSpecified = true;

                var unattendedDelivery = false;
                bool.TryParse(order.UnattendedDelivery, out unattendedDelivery);
                if (unattendedDelivery)
                {
                    salesTable.DeliveryName += "(Unattended)";
                    salesTable.SalesName   += "(Unattended)";
                }

                salesTable.BRIBypassDupPOCheck = AxdExtType_BRIBypassDupPOCheck.No;
                salesTable.SalesType = WS_SalesOrderService.AxdEnum_SalesType.Sales;
                salesTable.SalesOriginId = ax_SalesOriginId;

                salesTable.TECShippingUnit = "ea";

                if (order.Shipping >= 0)
                {

                    var markUpHeader = new WS_SalesOrderService.AxdEntity_MarkupTransHeader()
                                           {
                                               MarkupCode =ax_MarkupCode,
                                               MarkupCategory =WS_SalesOrderService.AxdExtType_MarkupCategoryType.Fixed,
                                               Value = order.Shipping,
                                               ValueSpecified = true,
                                               Keep =WS_SalesOrderService.AxdExtType_MarkupKeep.Yes,
                                               KeepSpecified = true,
                                               CurrencyCode =ax_CurrencyCode
                                           };



                    if (isAx_includeTaxHeader)
                    {
                        markUpHeader.TaxGroup = taxGroup;
                        markUpHeader.TaxItemGroup = ax_TaxItemGroup;
                    }

                    salesTable.MarkupTransHeader = new WS_SalesOrderService.AxdEntity_MarkupTransHeader[]{
                           markUpHeader
                        };

                    this.SendLogs(" salesTable.MarkupTransHeader ", salesTable.MarkupTransHeader);

                }

                if (isAx_includeTaxHeader)
                {
                    salesTable.TaxGroup = taxGroup;
                }


                if (order.LineItems != null)
                {
                    if (order.SalesDiscount > 0 && order.LineItems.Sum(e => e.Quantity) > 0)
                    {
                        var discountPerLine = (order.SalesDiscount / (decimal)order.LineItems.Count);

                        foreach (var orderItemLine in order.LineItems)
                        {
                            orderItemLine.Discount = Math.Round( (discountPerLine / orderItemLine.Quantity),2);
                        }

                        if (order.SalesDiscount != order.LineItems.Sum(e => e.Discount* e.Quantity))
                        {

                            var firstItem = order.LineItems.FirstOrDefault();
                            if (firstItem != null)
                            {
                                firstItem.Quantity--;
                                var discountDiff = order.SalesDiscount - order.LineItems.Sum(e => e.Discount* e.Quantity);
                                var lastItem = new OrderLine()
                                {
                                    Quantity = 1,
                                    Sku = firstItem.Sku,
                                    UnitPrice = firstItem.UnitPrice,
                                     Discount = discountDiff
                                };

                                order.LineItems.Add(lastItem);
                            }
                        }
                    }
                }


                var salesLines = new List<AxdEntity_SalesLine>();
                foreach (var orderItemLine in order.LineItems)
                {
                    var newSaleLine = new WS_SalesOrderService.AxdEntity_SalesLine
                    {
                        ItemId = orderItemLine.Sku,
                        SalesQty = orderItemLine.Quantity,
                        SalesUnit = "ea",
                        SalesGroup = "",
                        SalesPrice = orderItemLine.UnitPrice,
                        SalesPriceSpecified = true

                    };

                    if (orderItemLine.Discount > 0.0M)
                    {
                        newSaleLine.LineDisc = orderItemLine.Discount;
                        newSaleLine.LineDiscSpecified = true;
                        newSaleLine.BGTReasonCode = ax_BGTReasonCode;
                    }
                    salesLines.Add(newSaleLine);
                }

                salesTable.SalesLine = salesLines.ToArray();
                salesOrder.SalesTable = new WS_SalesOrderService.AxdEntity_SalesTable[1] { salesTable };

                try
                {
                    var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                    var valuesalesOrder = JsonConvert.SerializeObject(salesOrder);
                    defaultLogger.Information($"TO AX AIF ==> client.create(salesOrder) ==>: {valuesalesOrder}");

                    WS_SalesOrderService.EntityKey[] keys = client.create(salesOrder);
                    var orderStatus=new OrderStatus()
                                        {
                                            OrderId = order.OrderId,
                                            OrderNumber = order.OrderNumber,
                                            AxOrderNumber = keys[0].KeyData[0].Value,
                                            Status = "Processed"
                                        };
                    statuses.Add(orderStatus);
                    var valueSerialize = JsonConvert.SerializeObject(orderStatus);
                    defaultLogger.Warning($"AX SUCCES! ORDER STATUS : {valueSerialize}");
                }
                catch (Exception ex)
                {
                    var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                    defaultLogger.Error(ex, "AX AIF ERROR SubmitOrders");

                    Console.WriteLine("Error processing order {0}: {1}", order.OrderNumber, ex.Message);
                    statuses.Add(new OrderStatus() { OrderId = order.OrderId,  OrderNumber = order.OrderNumber, AxOrderNumber = "N/A", Status = "AX Failed: " + ex.Message + ex.InnerException });
                }
            }
            return statuses.ToArray();
        }

        private void OrderAlreadyExistsAction(List<OrderStatus> statuses, OrderSubmission order, string axOrderNumber)
        {
            this.SendLogs("ORDER EXIST", order);

            var orderStatus = new OrderStatus()
            {
                OrderNumber = order.OrderNumber,
                OrderId = order.OrderId,
                AxOrderNumber = axOrderNumber,
                Status = "Processed"
            };

            this.SendLogs("checkExistingOrder  EXIST", orderStatus);

            statuses.Add(orderStatus);
        }

        private WS_SalesOrderService.SalesOrderServiceClient SalesClient
        {
            get
            {
                var client = new WS_SalesOrderService.SalesOrderServiceClient();

                if (client.ClientCredentials == null) return client;
                client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                client.ClientCredentials.Windows.ClientCredential.UserName = @"britax\digitalresponse";
                client.ClientCredentials.Windows.ClientCredential.Password = "!QAZxsw2";

                return client;

            }
        }

        public OrderStatus GetOrderStatus(string orderNumber)
        {
            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
            WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record
            WS_SalesOrderService.EntityKey[] entityKey = { new WS_SalesOrderService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "SalesId";
            qe[0].Operator = WS_SalesOrderService.Operator.Equal;
            qe[0].Value1 = orderNumber;


            qc.CriteriaElement = qe;


            var client = this.SalesClient;
            var ax_endpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            OperationContextScope o = new OperationContextScope(client.InnerChannel);
            var mh = MessageHeader.CreateHeader("DestinationEndpoint", "http://schemas.microsoft.com/dynamics/2008/01/services", ax_endpoint);
            OperationContext.Current.OutgoingMessageHeaders.RemoveAll(mh.Name, mh.Namespace);
            OperationContext.Current.OutgoingMessageHeaders.Add(mh);


            try
            {

                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    WS_SalesOrderService.AxdSalesOrder order = client.read(entityKey);

                    if (order != null && order.SalesTable.Length > 0)
                    {
                        var table = order.SalesTable[0];
                        if (table.SalesStatus.HasValue)
                        {
                            status = table.SalesStatus.Value.ToString();
                        }
                        ship.Add(table.BEHDeliveryNum);
                        foreach (var line in table.SalesLine)
                        {
                            //if one line is delivered, mark the whole order delivered
                            if (line.SalesStatus == WS_SalesOrderService.AxdEnum_SalesStatus.Delivered)
                                status = line.SalesStatus.ToString();
                            ship.Add(line.BEHDeliveryNum);
                        }

                        var valueSerialize = JsonConvert.SerializeObject(order);
                        var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                        defaultLogger.Information($"GetOrderStatus {valueSerialize}");

                    }
                }
                else
                {
                    var valueSerialize = JsonConvert.SerializeObject($"NOT FOUND :{orderNumber} ");
                    var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                    defaultLogger.Information($"GetOrderStatus {valueSerialize}");
                }
            }
            catch (Exception exxxx)
            {
                var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                defaultLogger.Error(exxxx, "GetOrderStatus");

            }
            return new OrderStatus()
            {
                OrderNumber = orderNumber,
                Status = status,
                ShippingNumber = ship.Distinct()
            };
        }

        public OrderStatus GetOrderStatusByCustomerRef(string orderNumber)
        {
            string status = "Not Found";
            string axreference = "";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX
            WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
            WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record
            WS_SalesOrderService.EntityKey[] entityKey = { new WS_SalesOrderService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "customerref";
            qe[0].Operator = WS_SalesOrderService.Operator.Equal;
            qe[0].Value1 = orderNumber;

            qc.CriteriaElement = qe;



            var client = this.SalesClient;
            var ax_endpoint = WebConfigurationManager.AppSettings["ax_endpoint"].ToString();
            var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();

            try
            {

                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    WS_SalesOrderService.AxdSalesOrder order = client.read(entityKey);

                    if (order != null)
                    {
                        defaultLogger.Information($"FOUND  ORDER {orderNumber}");

                        var salesTableData = order.SalesTable.FirstOrDefault();
                        if (salesTableData != null)
                        {
                            axreference = salesTableData.SalesId;

                            status = salesTableData.SalesStatus?.ToString() ?? AxdEnum_SalesStatus.None.ToString();
                        }

                        defaultLogger.Information($"FOUND  ORDER {orderNumber}  {status}");

                    }
                    else
                    {
                        defaultLogger.Information($"NOT FOUND  ORDER orderNumber");
                        status = WS_SalesOrderService.AxdEnum_SalesStatus.Canceled.ToString();

                    }
                }
                else
                {
                    var valueSerialize = JsonConvert.SerializeObject($"NOT FOUND :{orderNumber}");

                    defaultLogger.Information($"GetOrderStatusByCustomerRef {valueSerialize}");

                    orderNumber = string.Empty;
                }
            }
            catch (Exception exxx)
            {

                defaultLogger.Error(exxx, "GetOrderStatus");

            }

            return new OrderStatus()
            {
                OrderNumber = orderNumber,
                Status = status,
                AxOrderNumber = axreference,
                ShippingNumber = ship.Distinct()
            };
        }
        public OrderStatus GetOrderByCustomer(string customerName)
        {

            string status = "Not Found";
            List<String> ship = new List<String>();
            // define QueryCriteria that will be used for finding data in AX

            WS_SalesOrderService.QueryCriteria qc = new WS_SalesOrderService.QueryCriteria();
            WS_SalesOrderService.CriteriaElement[] qe = { new WS_SalesOrderService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record
            WS_SalesOrderService.EntityKey[] entityKey = { new WS_SalesOrderService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "SalesTable";
            qe[0].FieldName = "DeliveryName";
            qe[0].Operator = WS_SalesOrderService.Operator.Equal;
            qe[0].Value1 = customerName;

            qc.CriteriaElement = qe;

            var client = SalesClient;

            try
            {

                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    WS_SalesOrderService.AxdSalesOrder order = client.read(entityKey);

                    if (order != null && order.SalesTable.Length > 0)
                    {
                        var table = order.SalesTable[0];
                        if (table.SalesStatus.HasValue)
                        {
                            status = table.SalesStatus.Value.ToString();
                        }
                        ship.Add(table.BEHDeliveryNum);
                        foreach (var line in table.SalesLine)
                        {
                            //if one line is delivered, mark the whole order delivered
                            if (line.SalesStatus == WS_SalesOrderService.AxdEnum_SalesStatus.Delivered) status = line.SalesStatus.ToString();
                            ship.Add(line.BEHDeliveryNum);
                        }

                        var valueSerialize = JsonConvert.SerializeObject(order);
                        var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                        defaultLogger.Information($"GetOrderByCustomer {valueSerialize}");

                    }
                }
                else
                {
                    var valueSerialize = JsonConvert.SerializeObject($"NOT FOUND :{customerName} ");
                    var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                    defaultLogger.Information($"GetOrderByCustomer {valueSerialize}");
                }
            }
            catch (Exception eyyy)
            {
                var defaultLogger = Bootstrapper.Container.Resolve<ILogger>();
                defaultLogger.Error(eyyy, "GetOrderByCustomer");
            }
            return new OrderStatus()
            {
                OrderNumber = customerName,
                Status = status,
                ShippingNumber = ship.Distinct()
            };

        }
        }


}
