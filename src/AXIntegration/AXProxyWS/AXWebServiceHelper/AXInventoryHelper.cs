﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
 

namespace AXWebServiceHelper
{
    using System.Web.Configuration;

    using Britax.Common.Models;

    public sealed  class AXInventoryHelper
    {

        private static readonly Lazy<AXInventoryHelper> lazy =
           new Lazy<AXInventoryHelper>(() => new AXInventoryHelper());

        public static AXInventoryHelper Instance { get { return lazy.Value; } }

        private WS_InventoryOnHandService.InventoryOnHandServiceClient InventoryClient
        {
            get
            {
                WS_InventoryOnHandService.InventoryOnHandServiceClient client = new WS_InventoryOnHandService.InventoryOnHandServiceClient();

                var ax_username = WebConfigurationManager.AppSettings["ax_username"].ToString();
                var ax_password = WebConfigurationManager.AppSettings["ax_password"].ToString();
                client.ClientCredentials.Windows.ClientCredential.UserName = ax_username;
                client.ClientCredentials.Windows.ClientCredential.Password = ax_password;

                client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
    

                return client;

            }
        }

        private WS_PriceListService.PriceListServiceClient PriceListClient
        {
            get
            {
                WS_PriceListService.PriceListServiceClient client = new WS_PriceListService.PriceListServiceClient();

                var ax_username = WebConfigurationManager.AppSettings["ax_username"].ToString();
                var ax_password = WebConfigurationManager.AppSettings["ax_password"].ToString();
                client.ClientCredentials.Windows.ClientCredential.UserName = ax_username;
                client.ClientCredentials.Windows.ClientCredential.Password = ax_password;

                client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
 
                return client;

            }
        }


        public InventoryLevel[] GetInventoryLevelsFromTA(List<InventoryLevel>  skus, string axAccountRelation, string axDataAreaId )
        {
            
            var ax_invent_dataareaid = WebConfigurationManager.AppSettings["ax_invent_dataareaid"].ToString();
            var ax_inventlocationid_list = WebConfigurationManager.AppSettings["ax_inventlocationid_list"].ToString();
            var ax_inventsiteid = WebConfigurationManager.AppSettings["ax_inventsiteid"].ToString();
            var ax_accountrelation_tradeagreement = WebConfigurationManager.AppSettings["ax_accountrelation_tradeagreement"].ToString();


            var levels = new List<InventoryLevel>();

            using (var dbAx = new AxExchange())
            {
//                var queryAxInventory = @"SELECT INVENTSUM.ITEMID as Sku, cast(sum(INVENTSUM.AVAILPHYSICAL) as decimal) as Inventory , cast(0 as decimal) as  Price,cast( '' as varchar(20)) as UmbracoNode FROM            INVENTSUM INNER JOIN                    INVENTDIM ON INVENTSUM.INVENTDIMID = INVENTDIM.INVENTDIMID where INVENTSUM.DATAAREAID='btx'   and inventlocationid in ('DIST', 'BSL','SPSUN') and   INVENTDIM.inventsiteid='SUN' group by INVENTSUM.ITEMID order by sku";

                var queryAxInventory = $"SELECT INVENTSUM.ITEMID as Sku, cast(sum(INVENTSUM.AVAILPHYSICAL) as decimal) as Inventory , cast(0 as decimal) as  Price,cast( '' as varchar(20)) as UmbracoNode FROM            INVENTSUM INNER JOIN                    INVENTDIM ON INVENTSUM.INVENTDIMID = INVENTDIM.INVENTDIMID where INVENTSUM.DATAAREAID='{ax_invent_dataareaid}'   and inventlocationid in ({ax_inventlocationid_list}) and   INVENTDIM.inventsiteid='{ax_inventsiteid}' group by INVENTSUM.ITEMID order by sku";


                var listItemInventoryAx = dbAx.Database.SqlQuery<InventoryLevel>(queryAxInventory).ToList();


                foreach (var sku in skus)
                {

                    var price =
                        dbAx.PRICEDISCTABLEs.FirstOrDefault(
                            e => e.ITEMRELATION == sku.Sku && e.ACCOUNTRELATION == ax_accountrelation_tradeagreement && e.DATAAREAID == ax_invent_dataareaid);

                    if (price != null)
                    {
                        sku.Inventory = listItemInventoryAx.Find(x => x.Sku == sku.Sku).Inventory;
                        sku.Price = price.AMOUNT;
                        levels.Add(sku);
                    }
                }
            }
       
            return levels.ToArray();
        }

		public SkuWithName[] GetInventorySkus(int batchNumber = 0, int batchSize = 0)
		{

            var ax_invent_dataareaid = WebConfigurationManager.AppSettings["ax_invent_dataareaid"].ToString();
            var ax_inventlocationid_list = WebConfigurationManager.AppSettings["ax_inventlocationid_list"].ToString();
            var ax_inventsiteid = WebConfigurationManager.AppSettings["ax_inventsiteid"].ToString();
            var ax_accountrelation_tradeagreement = WebConfigurationManager.AppSettings["ax_accountrelation_tradeagreement"].ToString();
        

            var skuList = new List<SkuWithName>();

            using (var dbAx = new AxExchange())
            {
                var queryAxInventory = $"SELECT INVENTSUM.ITEMID AS Sku , MIN(INVENTTABLE.ITEMNAME) AS Name FROM INVENTSUM INNER JOIN INVENTDIM ON INVENTSUM.INVENTDIMID = INVENTDIM.INVENTDIMID LEFT OUTER JOIN INVENTTABLE ON INVENTSUM.ITEMID = INVENTTABLE.ITEMID WHERE INVENTSUM.DATAAREAID = '{ax_invent_dataareaid}' AND inventlocationid IN ({ax_inventlocationid_list}) AND INVENTDIM.inventsiteid = '{ax_inventsiteid}' GROUP BY INVENTSUM.ITEMID ORDER BY Sku";


                skuList = dbAx.Database.SqlQuery<SkuWithName>(queryAxInventory).ToList();

				skuList = skuList.Skip(batchNumber * batchSize).ToList();
				if (batchSize > 0)
				{
					skuList = skuList.Take(batchSize).ToList();
				}
            }
			
            return skuList.ToArray();
		}

		public InventoryLevel[] GetInventoryLevels(string[] skus)
        {
            // define client
            var client = this.InventoryClient;

            List<InventoryLevel> levels = new List<InventoryLevel>();

            foreach (string sku in skus)
            {
                WS_InventoryOnHandService.QueryCriteria qc = new WS_InventoryOnHandService.QueryCriteria();
                WS_InventoryOnHandService.CriteriaElement[] qe = { new WS_InventoryOnHandService.CriteriaElement() };

                // define entity key that will contain value of unique identifier of a record 
                WS_InventoryOnHandService.EntityKey[] entityKey = { new WS_InventoryOnHandService.EntityKey() };

                // example of criteria element for a query
                qe[0].DataSourceName = "InventSum";
                qe[0].FieldName = "ItemId";
                qe[0].Operator = WS_InventoryOnHandService.Operator.Equal;
                qe[0].Value1 = sku;

                qc.CriteriaElement = qe;


                try
                {

                    // findKeys service operation will return list of unique fields that meet query criteria
                    entityKey = client.findKeys(qc);
                    if (null != entityKey)
                    {
                        var onhand = client.read(entityKey);
                        decimal inventory = onhand.InventSum.Where(i => i.InventDim.Any(d => d.InventLocationId == "DIST")).Sum(i => (i.AvailPhysicalSpecified) ? i.AvailPhysical.Value : 0.0M);
                        levels.Add(new InventoryLevel()
                        {
                            Sku = sku,
                            Inventory = inventory
                        });
                    }
                    else
                    {
                        //Since we couldn't find the Item by Model Number, we know that AX doesn't have this model in its system so we want to 
                        //mark it as out of stock.
                        levels.Add(new InventoryLevel()
                        {
                            Sku = sku,
                            Inventory = 0M
                        });
                    }
                }
                catch (Exception)
                {
                    levels.Add(new InventoryLevel()
                    {
                        Sku = sku,
                        Inventory = -1.0M
                    });
                }
            }

            return levels.ToArray();
        }

      
        
        public InventoryLevel GetInventoryLevel(string sku)
        {

            var level = new InventoryLevel() { };
            level.Sku = sku;
            level.Inventory = -1.0M;
            // define client
            var client = InventoryClient;

            // define QueryCriteria that will be used for finding data in AX

            WS_InventoryOnHandService.QueryCriteria qc = new WS_InventoryOnHandService.QueryCriteria();
            WS_InventoryOnHandService.CriteriaElement[] qe = { new WS_InventoryOnHandService.CriteriaElement() };

            // define entity key that will contain value of unique identifier of a record 
            WS_InventoryOnHandService.EntityKey[] entityKey = { new WS_InventoryOnHandService.EntityKey() };

            // example of criteria element for a query
            qe[0].DataSourceName = "InventSum";
            qe[0].FieldName = "ItemId";
            qe[0].Operator = WS_InventoryOnHandService.Operator.Equal;
            qe[0].Value1 = sku;

            qc.CriteriaElement = qe;

            try
            {
                // findKeys service operation will return list of unique fields that meet query criteria
                entityKey = client.findKeys(qc);
                if (null != entityKey)
                {
                    var onhand = client.read(entityKey);
                    level.Inventory = onhand.InventSum.Where(i => i.InventDim.Any(d => d.InventLocationId == "DIST")).Sum(i => (i.AvailPhysicalSpecified) ? i.AvailPhysical.Value : 0.0M);
                }

            }
            catch (Exception exx)
            {
                level = new InventoryLevel()
                {
                    Sku = sku,
                    Inventory = -1.0M
                };
            }
            client.Close();

            return level;

        }

        //public InventoryLevel GetInventoryLevelFromTa(string accountCode,string sku)
        //{

        //    var level = new InventoryLevel() { };
        //    level.Sku = sku;
        //    level.Inventory = -1.0M;
        //    // define client
        //    var client = PriceListClient;

        //    // define QueryCriteria that will be used for finding data in AX
            


        //    WS_PriceListService.QueryCriteria qc = new WS_PriceListService.QueryCriteria();
        //    WS_PriceListService.CriteriaElement[] qe = { new WS_PriceListService.CriteriaElement() };

        //    // define entity key that will contain value of unique identifier of a record 
        //   // WS_PriceListService.EntityKey.EntityKey[] entityKey = { new WS_PriceListService.EntityKey() };

        //    // example of criteria element for a query
        //    qe[0].DataSourceName = "AxPriceDiscTmpPrintout";
        //    qe[0].FieldName = "ItemId";
        //    qe[0].Operator = WS_PriceListService.Operator.Equal;
        //    qe[0].Value1 = "0035";

        //    qc.CriteriaElement = qe;

        //    try
        //    {
        //        // findKeys service operation will return list of unique fields that meet query criteria
        //        var pricelist = client.find(qc);
                

        //    }
        //    catch (Exception exx)
        //    {
        //        level = new InventoryLevel()
        //        {
        //            Sku = sku,
        //            Inventory = -1.0M
        //        };
        //    }
        //    client.Close();

        //    return level;

        //}

    }
}
