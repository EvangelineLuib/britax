﻿namespace Britax.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class OrderSubmission
    {
        List<OrderLine> lineItems = null;
         
        [DataMember]
        public string OrderNumber { get; set; } = "";

        [DataMember]
        public Guid  OrderId { get; set; } 

        [DataMember]
        public string CurrencyCode { get; set; } = "USD";

        [DataMember]
        public string AuthorizationId { get; set; } = "";

        [DataMember]
        public string Account { get; set; } = "1101";

        [DataMember]
        public string ShipName { get; set; } = "";

        [DataMember]
        public string ShipAddress { get; set; } = "";

        [DataMember]
        public string ShipStreet { get; set; } = "";

        [DataMember]
        public string ShipCity { get; set; } = "";

        [DataMember]
        public string ShipState { get; set; } = "";

        [DataMember]
        public string ShipZip { get; set; } = "";

        [DataMember]
        public string Email { get; set; } = "";

        [DataMember]
        public DateTime ShipDate { get; set; } = DateTime.Now;

        [DataMember]
        public string ShipPhone { get; set; } = "";

        [DataMember]
        public string UnattendedDelivery { get; set; }

        [DataMember]
        public string PaymentMode { get; set; } = "";

        [DataMember]
        public string OrderType { get; set; } = "";

        [DataMember]
        public decimal Shipping { get; set; } = 0.0M;

        [DataMember]
        public decimal SalesTax { get; set; } = 0.0M;


        [DataMember]
        public decimal SalesDiscount { get; set; } = 0.0M;


        [DataMember]
        public List<OrderLine> LineItems
        {
            get { return this.lineItems ?? (this.lineItems = new List<OrderLine>()); }
            set { this.lineItems = value; }
        }


    }
}
