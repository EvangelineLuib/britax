﻿namespace Britax.Common.Models
{
    using System.Runtime.Serialization;

    [DataContract]
    public class OrderLine
    {
        [DataMember]
        public string Sku { get; set; } = "";

        [DataMember]
        public decimal Quantity { get; set; } = 0;

        [DataMember]
        public decimal UnitPrice { get; set; } = 0.0M;

        [DataMember]
        public decimal Discount { get; set; } = 0.0M;

        [DataMember]
        public string DiscountCode { get; set; } = "";
    }

}
