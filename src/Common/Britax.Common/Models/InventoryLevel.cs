﻿using System.Runtime.Serialization;

namespace Britax.Common.Models
{
    [DataContract]
    public class InventoryLevel
    {
        [DataMember]
        public string Sku { get; set; } = "";

        [DataMember]
        public decimal  Inventory { get; set; } = 0; 

        [DataMember]
        public string  UmbracoNode { get; set; }

        [DataMember]
        public decimal Price { get; set; } = 0;
    }
}
