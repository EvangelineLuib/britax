﻿namespace Britax.Common.Models
{
    using System.Runtime.Serialization;

    [DataContract]
	public class SkuWithName
	{
		[DataMember]
		public string Sku { get; set; }

		[DataMember]
		public string Name { get; set; }
	}
}
