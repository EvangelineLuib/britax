﻿namespace Britax.Common.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [DataContract]
    public class OrderStatus
    {
        [DataMember]
        public string Status { get; set; } = "";

        [DataMember]
        public string OrderNumber { get; set; } = "";

        [DataMember]
        public Guid  OrderId { get; set; }  


        [DataMember]
        public string AxOrderNumber { get; set; } = "";


        [DataMember]
        public IEnumerable<string> ShippingNumber { get; set; } = new List<string>();
    }
}
